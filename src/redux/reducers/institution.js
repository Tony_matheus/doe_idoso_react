import {
  INSTITUTION_GET_ALL,
  INSTITUTION_GET_ALL_FAILURE,
  INSTITUTION_GET_ALL_SUCCESS,
  INSTITUTION_CREATE,
  INSTITUTION_CREATE_FAILURE,
  INSTITUTION_CREATE_SUCCESS,
  INSTITUTION_DELETE,
  INSTITUTION_DELETE_FAILURE,
  INSTITUTION_DELETE_SUCCESS
} from "../actionTypes";

const initialState = {
  isGettingAll: false,
  isCreating: false,
  isDeleting: false,
  all: [],
  error: ""
}

export default function (state = initialState, action) {
  switch (action.type) {
    case INSTITUTION_GET_ALL: {
      return {
        ...state,
        isGettingAll: true
      }
    }
    case INSTITUTION_GET_ALL_FAILURE: {
      return {
        ...state,
        isGettingAll: false,
        error: action.payload
      }
    }
    case INSTITUTION_GET_ALL_SUCCESS: {
      return {
        ...state,
        isGettingAll: false,
        all: action.payload
      }
    }
    case INSTITUTION_CREATE: {
      return {
        ...state,
        isCreating: true
      }
    }
    case INSTITUTION_CREATE_FAILURE: {
      return {
        ...state,
        isCreating: false,
        error: action.payload
      }
    }
    case INSTITUTION_CREATE_SUCCESS: {
      return {
        ...state,
        isCreating: false,
        all: [
          ...state.all,
          action.payload
        ]
      }
    }
    case INSTITUTION_DELETE: {
      return {
        ...state,
        isDeleting: true
      }
    }
    case INSTITUTION_DELETE_FAILURE: {
      return {
        ...state,
        isDeleting: false,
        error: action.payload
      }
    }
    case INSTITUTION_DELETE_SUCCESS: {
      const institutions = state.all.filter(institution => institution.id !== action.payload)
      return {
        ...state,
        isDeleting: false,
        all: institutions
      }
    }
    default:
      return state
  }
}
