import {
  DONATE_GET_ALL,
  DONATE_GET_ALL_FAILURE,
  DONATE_GET_ALL_SUCCESS,
  DONATE_CREATE,
  DONATE_CREATE_FAILURE,
  DONATE_CREATE_SUCCESS,
  DONATE_DELETE,
  DONATE_DELETE_FAILURE,
  DONATE_DELETE_SUCCESS
} from "../actionTypes";

const initialState = {
  isGettingAll: false,
  isCreating: false,
  isDeleting: false,
  all: [],
  error: ""
}

export default function (state = initialState, action) {
  switch (action.type) {
    case DONATE_GET_ALL: {
      return {
        ...state,
        isGettingAll: true
      }
    }
    case DONATE_GET_ALL_FAILURE: {
      return {
        ...state,
        isGettingAll: false,
        error: action.payload
      }
    }
    case DONATE_GET_ALL_SUCCESS: {
      return {
        ...state,
        isGettingAll: false,
        all: action.payload
      }
    }
    case DONATE_CREATE: {
      return {
        ...state,
        isCreating: true
      }
    }
    case DONATE_CREATE_FAILURE: {
      return {
        ...state,
        isCreating: false,
        error: action.payload
      }
    }
    case DONATE_CREATE_SUCCESS: {
      return {
        ...state,
        isCreating: false,
        all: [
          ...state.all,
          action.payload
        ]
      }
    }
    case DONATE_DELETE: {
      return {
        ...state,
        isDeleting: true
      }
    }
    case DONATE_DELETE_FAILURE: {
      return {
        ...state,
        isDeleting: false,
        error: action.payload
      }
    }
    case DONATE_DELETE_SUCCESS: {
      const institutions = state.all.filter(institution => institution.id !== action.payload)
      return {
        ...state,
        isDeleting: false,
        all: institutions
      }
    }
    default:
      return state
  }
}
