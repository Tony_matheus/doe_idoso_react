import { combineReducers } from 'redux'

import user from './user'
import institution from './institution'
import project from './project'
import article from './article'
import donate from './donate'
console.log(article, project)
export default combineReducers({
  user,
  institution,
  project,
  article,
  donate
})
