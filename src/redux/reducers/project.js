import {
  PROJECT_GET_ALL,
  PROJECT_GET_ALL_FAILURE,
  PROJECT_GET_ALL_SUCCESS,
  PROJECT_CREATE,
  PROJECT_CREATE_FAILURE,
  PROJECT_CREATE_SUCCESS,
  PROJECT_DELETE,
  PROJECT_DELETE_FAILURE,
  PROJECT_DELETE_SUCCESS
} from "../actionTypes";

const initialState = {
  isGettingAll: false,
  isCreating: false,
  isDeleting: false,
  all: [],
  error: ""
}

export default function (state = initialState, action) {
  switch (action.type) {
    case PROJECT_GET_ALL: {
      return {
        ...state,
        isGettingAll: true
      }
    }
    case PROJECT_GET_ALL_FAILURE: {
      return {
        ...state,
        isGettingAll: false,
        error: action.payload
      }
    }
    case PROJECT_GET_ALL_SUCCESS: {
      return {
        ...state,
        isGettingAll: false,
        all: action.payload
      }
    }
    case PROJECT_CREATE: {
      return {
        ...state,
        isCreating: true
      }
    }
    case PROJECT_CREATE_FAILURE: {
      return {
        ...state,
        isCreating: false,
        error: action.payload
      }
    }
    case PROJECT_CREATE_SUCCESS: {
      return {
        ...state,
        isCreating: false,
        all: [
          ...state.all,
          action.payload
        ]
      }
    }
    case PROJECT_DELETE: {
      return {
        ...state,
        isDeleting: true
      }
    }
    case PROJECT_DELETE_FAILURE: {
      return {
        ...state,
        isDeleting: false,
        error: action.payload
      }
    }
    case PROJECT_DELETE_SUCCESS: {
      const projects = state.all.filter(project => project.id !== action.payload)
      return {
        ...state,
        isDeleting: false,
        all: projects
      }
    }
    default:
      return state
  }
}
