import {
  USER_AUTHENTICATE,
  USER_AUTHENTICATE_SUCCESS,
  USER_AUTHENTICATE_FAILURE,
  USER_REGISTER,
  USER_REGISTER_FAILURE,
  USER_REGISTER_SUCCESS,
  USER_TOKEN_INVALID,
  USER_CHECK_TOKEN,
  USER_TOKEN_VALID,
  USER_GET_ALL,
  USER_GET_ALL_SUCCESS,
  USER_GET_ALL_FAILURE,
  LOGOUT,
  USER_DELETE,
  USER_DELETE_FAILURE,
  USER_DELETE_SUCCESS
} from '../actionTypes'

const initialState = {
  isAuthenticated: !!localStorage.getItem('access-token'),
  isAuthenticating: false,
  isGettingAll: false,
  isCreating: false,
  isDeleting: false,
  registered: false,
  error: "",
  all: [],
  role: null
}
export default function (state = initialState, action) {
  switch (action.type) {
    case USER_AUTHENTICATE: {
      return {
        ...state,
        isAuthenticating: true,
        isAuthenticated: false,
        didFirstCheck: false,
        role: null,
        error: ''
      }
    }
    case USER_AUTHENTICATE_FAILURE: {
      return {
        ...state,
        isAuthenticating: false,
        error: action.payload
      }
    }
    case USER_AUTHENTICATE_SUCCESS: {
      return {
        ...state,
        isAuthenticating: false,
        isAuthenticated: true,
        role: action.payload
      }
    }
    case USER_GET_ALL: {
      return {
        ...state,
        isGettingAll: true,
        error: ''
      }
    }
    case USER_GET_ALL_FAILURE: {
      return {
        ...state,
        isGettingAll: false,
        error: action.payload
      }
    }
    case USER_GET_ALL_SUCCESS: {
      return {
        ...state,
        isGettingAll: false,
        all: action.payload
      }
    }
    case USER_REGISTER: {
      return {
        ...state,
        isAuthenticating: true,
        registered: false,
        didFirstCheck: false,
        error: ''
      }
    }
    case USER_REGISTER_FAILURE: {
      return {
        ...state,
        isAuthenticating: false,
        registered: false,
        error: action.payload
      }
    }
    case USER_REGISTER_SUCCESS: {
      return {
        ...state,
        isAuthenticating: false,
        registered: true
      }
    }
    case USER_CHECK_TOKEN: {
      return {
        ...state,
        isCheckingToken: true,
        error: ''
      }
    }
    case USER_TOKEN_VALID: {
      return {
        ...state,
        isCheckingToken: false,
        isAuthenticated: true,
        didFirstCheck: true,
        error: ''
      }
    } case USER_TOKEN_INVALID: {
      return {
        ...initialState,
        isAuthenticated: false,
        didFirstCheck: true,
        error: action.payload
      }
    } case LOGOUT: {
      return {
        ...initialState,
        isAuthenticated: false,
      }
    } case USER_DELETE: {
      return {
        ...state,
        isDeleting: true
      }
    }
    case USER_DELETE_FAILURE: {
      return {
        ...state,
        isDeleting: false,
        error: action.payload
      }
    }
    case USER_DELETE_SUCCESS: {
      const users = state.all.filter(user => user.id !== action.payload)
      return {
        ...state,
        isDeleting: false,
        all: users
      }
    }
    default:
      return state
  }
}
