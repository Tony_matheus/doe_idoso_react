import {
    ARTICLE_GET_ALL,
    ARTICLE_GET_ALL_FAILURE,
    ARTICLE_GET_ALL_SUCCESS,
    ARTICLE_CREATE,
    ARTICLE_CREATE_FAILURE,
    ARTICLE_CREATE_SUCCESS,
    ARTICLE_DELETE,
    ARTICLE_DELETE_FAILURE,
    ARTICLE_DELETE_SUCCESS
  } from "../actionTypes";
  
  const initialState = {
    isGettingAll: false,
    isCreating: false,
    isDeleting: false,
    all: [],
    error: ""
  }
  
  export default function (state = initialState, action) {
    switch (action.type) {
      case ARTICLE_GET_ALL: {
        return {
          ...state,
          isGettingAll: true
        }
      }
      case ARTICLE_GET_ALL_FAILURE: {
        return {
          ...state,
          isGettingAll: false,
          error: action.payload
        }
      }
      case ARTICLE_GET_ALL_SUCCESS: {
        return {
          ...state,
          isGettingAll: false,
          all: action.payload
        }
      }
      case ARTICLE_CREATE: {
        return {
          ...state,
          isCreating: true
        }
      }
      case ARTICLE_CREATE_FAILURE: {
        return {
          ...state,
          isCreating: false,
          error: action.payload
        }
      }
      case ARTICLE_CREATE_SUCCESS: {
        return {
          ...state,
          isCreating: false,
          all: [
            ...state.all,
            action.payload
          ]
        }
      }
      case ARTICLE_DELETE: {
        return {
          ...state,
          isDeleting: true
        }
      }
      case ARTICLE_DELETE_FAILURE: {
        return {
          ...state,
          isDeleting: false,
          error: action.payload
        }
      }
      case ARTICLE_DELETE_SUCCESS: {
        const articles = state.all.filter(article => article.id !== action.payload)
        return {
          ...state,
          isDeleting: false,
          all: articles
        }
      }
      default:
        return state
    }
  }
  