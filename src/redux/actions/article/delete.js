import axios from 'axios'

import {API, getHeaders} from '../../../config'
import {handleHttpError} from '../../../utils'

import {
  ARTICLE_DELETE,
  ARTICLE_DELETE_FAILURE,
  ARTICLE_DELETE_SUCCESS
} from '../../actionTypes'

export default (id) => async dispatch => {
  try {
    dispatch({type: ARTICLE_DELETE})


    const headers = getHeaders()
    const response = await axios.delete(`${API.url}/api/v1/article/${id}`, {headers: headers})
    dispatch({
      type: ARTICLE_DELETE_SUCCESS,
      payload: id
    })
  } catch (err) {
    const errorMessage = handleHttpError(err)

    console.log(err.response)
    dispatch({
      type: ARTICLE_DELETE_FAILURE,
      payload: errorMessage
    })
  }
}
