import axios from 'axios'

import { API, getHeaders } from '../../../config'
import { handleHttpError } from '../../../utils'

import {
  ARTICLE_GET_ALL,
  ARTICLE_GET_ALL_FAILURE,
  ARTICLE_GET_ALL_SUCCESS
} from '../../actionTypes'

export default () => async dispatch => {
  try {
    dispatch({ type: ARTICLE_GET_ALL })

    const headers = getHeaders()
    const response = await axios.get(`${API.url}/api/v1/article`, {headers: headers})
    dispatch({
      type: ARTICLE_GET_ALL_SUCCESS,
      payload: response.data
    })
  }
  catch(err) {
    const errorMessage = handleHttpError(err)

    dispatch({
      type: ARTICLE_GET_ALL_FAILURE,
      payload: errorMessage
    })
  }
}
