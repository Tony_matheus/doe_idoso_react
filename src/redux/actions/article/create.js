import axios from 'axios'

import { API, getHeaders } from '../../../config'
import {handleHttpError} from '../../../utils'

import {
  ARTICLE_CREATE,
  ARTICLE_CREATE_FAILURE,
  ARTICLE_CREATE_SUCCESS
} from '../../actionTypes'

export default (data) => async dispatch => {
  try {
    dispatch({type: ARTICLE_CREATE})
    const body = {
      article: {
        title: data.title,
        text: data.text,
      }
    }

    const headers = getHeaders()

    const response = await axios.post(`${API.url}/api/v1/article`, body, {headers: headers})
    console.log(response.data)
    dispatch({
      type: ARTICLE_CREATE_SUCCESS,
      payload: response.data
    })
  } catch (err) {
    const errorMessage = handleHttpError(err)

    console.log(err.response)
    dispatch({
      type: ARTICLE_CREATE_FAILURE,
      payload: errorMessage
    })
  }
}
