import getAll from './getAll'
import create from './create'
import deleteArticle from './delete'

export {
  getAll,
  create,
  deleteArticle
}
