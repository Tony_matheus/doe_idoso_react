import axios from 'axios'

import { API, getHeaders } from '../../../config'
import { handleHttpError } from '../../../utils'

import {
  INSTITUTION_GET_ALL,
  INSTITUTION_GET_ALL_FAILURE,
  INSTITUTION_GET_ALL_SUCCESS
} from '../../actionTypes'

export default () => async dispatch => {
  try {
    dispatch({ type: INSTITUTION_GET_ALL })
    const headers = getHeaders();
    const response = await axios.get(`${API.url}/api/v1/institution`, {headers: headers})
    dispatch({
      type: INSTITUTION_GET_ALL_SUCCESS,
      payload: response.data
    })
  }
  catch(err) {
    const errorMessage = handleHttpError(err)

    dispatch({
      type: INSTITUTION_GET_ALL_FAILURE,
      payload: errorMessage
    })
  }
}
