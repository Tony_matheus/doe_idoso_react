import getAll from './getAll'
import create from './create'
import deleteInstitution from './delete'

export {
  getAll,
  create,
  deleteInstitution
}
