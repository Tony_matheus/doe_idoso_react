import axios from 'axios'

import {API, getHeaders} from '../../../config'
import {handleHttpError} from '../../../utils'

import {
  INSTITUTION_DELETE,
  INSTITUTION_DELETE_FAILURE,
  INSTITUTION_DELETE_SUCCESS
} from '../../actionTypes'

export default (id) => async dispatch => {
  try {
    dispatch({type: INSTITUTION_DELETE})

    const headers = getHeaders()
    const response = await axios.delete(`${API.url}/api/v1/institution/${id}`, {headers: headers})
    dispatch({
      type: INSTITUTION_DELETE_SUCCESS,
      payload: id
    })
  } catch (err) {
    const errorMessage = handleHttpError(err)

    console.log(err.response)
    dispatch({
      type: INSTITUTION_DELETE_FAILURE,
      payload: errorMessage
    })
  }
}
