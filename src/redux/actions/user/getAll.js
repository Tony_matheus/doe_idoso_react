import axios from 'axios'

import { API, getHeaders, setHeaders } from '../../../config'
import { handleHttpError } from '../../../utils'

import {
  USER_GET_ALL,
  USER_GET_ALL_FAILURE,
  USER_GET_ALL_SUCCESS
} from '../../actionTypes'

export default () => async dispatch => {
  try {
    dispatch({ type: USER_GET_ALL })
    const headers = getHeaders();
    const response = await axios.get(`${API.url}/api/v1/user`, { headers });

    setHeaders(response.headers);

    dispatch({
      type: USER_GET_ALL_SUCCESS,
      payload: response.data
    })
  }
  catch (err) {
    const errorMessage = handleHttpError(err)

    dispatch({
      type: USER_GET_ALL_FAILURE,
      payload: errorMessage
    })
  }
}
