import authenticate from './authenticate';
import register from './register';
import checkToken from './checkToken';
import logout from './logout';
import getAll from './getAll';
import deleteUser from './delete';

export {
  authenticate,
  register,
  checkToken,
  logout,
  getAll,
  deleteUser
}