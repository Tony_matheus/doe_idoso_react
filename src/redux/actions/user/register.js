import axios from 'axios'

import { API, setHeaders } from '../../../config'
import { handleHttpError } from '../../../utils'

import {
  USER_REGISTER,
  USER_REGISTER_FAILURE,
  USER_REGISTER_SUCCESS
} from '../../actionTypes'

export default ({ name, email, password, password_confirmation }) => async dispatch => {
  try {
    dispatch({ type: USER_REGISTER })

    const body = {
      "user": {
        "type_person": 1,
        "type_user": 1,
        "name": name || "Eurico Lopes",
        "cpf_cnpj": "872.351.880-49",
        email, password, password_confirmation,
        "address_attributes": {

          "postal_code": "60040-480",
          "district": "fátima",
          "number": "859",
          "state": "ce",
          "street": "rua",
          "zip_code": "60"
        },
        "contacts_attributes": [
          {
            "contact_name": "mateus",
            "email": "mateus@gmail.com",
            "phone": "989943409",
            "areaCode": "85"
          }
        ]
      }
    }
    const response = await axios.post(`${API.url}/api/v1/user`, body);

    setHeaders(response.headers);

    dispatch({
      type: USER_REGISTER_SUCCESS,
      payload: {}
    })
  }
  catch (err) {
    const errorMessage = handleHttpError(err)

    dispatch({
      type: USER_REGISTER_FAILURE,
      payload: errorMessage
    })
  }
}
