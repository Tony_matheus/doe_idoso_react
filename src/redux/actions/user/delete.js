import axios from 'axios'

import {API, getHeaders} from '../../../config'
import {handleHttpError} from '../../../utils'

import {
  USER_DELETE,
  USER_DELETE_FAILURE,
  USER_DELETE_SUCCESS
} from '../../actionTypes'

export default (id) => async dispatch => {
  try {
    dispatch({type: USER_DELETE})

    const headers = getHeaders();
    const response = await axios.delete(`${API.url}/api/v1/user/${id}`, {headers: headers})
    dispatch({
      type: USER_DELETE_SUCCESS,
      payload: id
    })
  } catch (err) {
    const errorMessage = handleHttpError(err)

    console.log(err.response)
    dispatch({
      type: USER_DELETE_FAILURE,
      payload: errorMessage
    })
  }
}
