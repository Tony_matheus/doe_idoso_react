import axios from 'axios'

import { API } from '../../../config'

import { 
  USER_CHECK_TOKEN,
  USER_TOKEN_VALID,
  USER_TOKEN_INVALID
} from '../../actionTypes'

export default () => async dispatch => {
  try {
    dispatch({ type: USER_CHECK_TOKEN })
    
    const headers = { 'Authorization': localStorage.getItem('access-token') }
    
    // await axios.get(`${API.url}/check_token`, { headers })

    dispatch({ type: USER_TOKEN_VALID })
  }
  catch(err) {
    localStorage.removeItem('userRole')
    localStorage.removeItem('token')
    
    dispatch({ type: USER_TOKEN_INVALID })
  }
}