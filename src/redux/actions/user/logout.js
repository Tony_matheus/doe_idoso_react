import axios from 'axios'

import { API } from '../../../config'

import { 
  LOGOUT
} from '../../actionTypes'

export default () => async dispatch => {
  try {
    dispatch({ type: LOGOUT })
  }
  catch(err) {
    dispatch({ type: LOGOUT })
  }
}