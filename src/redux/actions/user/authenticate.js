import axios from 'axios'

import { API, setHeaders } from '../../../config'
import { handleHttpError } from '../../../utils'

import {
  USER_AUTHENTICATE,
  USER_AUTHENTICATE_FAILURE,
  USER_AUTHENTICATE_SUCCESS
} from '../../actionTypes'

export default ({ email, password }) => async dispatch => {
  try {
    dispatch({ type: USER_AUTHENTICATE })

    const body = { email, password };
    const response = await axios.post(`${API.url}/auth/sign_in`, body);
    
    setHeaders(response.headers);

    dispatch({
      type: USER_AUTHENTICATE_SUCCESS,
      payload: {  }
    })
  }
  catch(err) {
    const errorMessage = handleHttpError(err)

    dispatch({
      type: USER_AUTHENTICATE_FAILURE,
      payload: errorMessage
    })
  }
}
