import axios from 'axios'

import {API, getHeaders} from '../../../config'
import {handleHttpError} from '../../../utils'

import {
  PROJECT_DELETE,
  PROJECT_DELETE_FAILURE,
  PROJECT_DELETE_SUCCESS
} from '../../actionTypes'

export default (id) => async dispatch => {
  try {
    dispatch({type: PROJECT_DELETE})

    const headers = getHeaders();
    const response = await axios.delete(`${API.url}/api/v1/project/${id}`, {headers: headers})
    dispatch({
      type: PROJECT_DELETE_SUCCESS,
      payload: id
    })
  } catch (err) {
    const errorMessage = handleHttpError(err)

    console.log(err.response)
    dispatch({
      type: PROJECT_DELETE_FAILURE,
      payload: errorMessage
    })
  }
}
