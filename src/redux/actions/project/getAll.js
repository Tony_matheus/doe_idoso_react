import axios from 'axios'

import { API, getHeaders } from '../../../config'
import { handleHttpError } from '../../../utils'

import {
  PROJECT_GET_ALL,
  PROJECT_GET_ALL_FAILURE,
  PROJECT_GET_ALL_SUCCESS
} from '../../actionTypes'

export default () => async dispatch => {
  try {
    dispatch({ type: PROJECT_GET_ALL })
    const headers = getHeaders();
    const response = await axios.get(`${API.url}/api/v1/project`, {headers: headers})
    dispatch({
      type: PROJECT_GET_ALL_SUCCESS,
      payload: response.data
    })
  }
  catch(err) {
    const errorMessage = handleHttpError(err)

    dispatch({
      type: PROJECT_GET_ALL_FAILURE,
      payload: errorMessage
    })
  }
}
