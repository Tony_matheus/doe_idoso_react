import axios from 'axios'

import {API, getHeaders} from '../../../config'
import {handleHttpError} from '../../../utils'

import {
  PROJECT_CREATE,
  PROJECT_CREATE_FAILURE,
  PROJECT_CREATE_SUCCESS
} from '../../actionTypes'

export default (data) => async dispatch => {
  try {
    dispatch({type: PROJECT_CREATE})
    const body = {
      project: {
        name: data.name,
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
        address_attributes: {
          neighborhood: data.neighborhood,
          city: data.city,
          complement: data.complement,
          district: data.district,
          postal_code: data.postal_code,
          number: data.number,
          state: data.state,
          street: data.street,
          zip_code: data.zip_code
        },
        contacts_attributes: [{
            contact_name: data.contact_name,
            email: data.email,
            phone: data.phone,
            areaCode: data.areaCode
          }]
      }
    }

    const headers = getHeaders();
    const response = await axios.post(`${API.url}/api/v1/project`, body, {headers: headers})
    console.log(response.data)
    dispatch({
      type: PROJECT_CREATE_SUCCESS,
      payload: response.data
    })
  } catch (err) {
    const errorMessage = handleHttpError(err)

    console.log(err.response)
    dispatch({
      type: PROJECT_CREATE_FAILURE,
      payload: errorMessage
    })
  }
}
