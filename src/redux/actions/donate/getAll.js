import axios from 'axios'

import { API, getHeaders } from '../../../config'
import { handleHttpError } from '../../../utils'

import {
  DONATE_GET_ALL,
  DONATE_GET_ALL_FAILURE,
  DONATE_GET_ALL_SUCCESS
} from '../../actionTypes'

export default () => async dispatch => {
  try {
    dispatch({ type: DONATE_GET_ALL })
    const headers = getHeaders();
    const response = await axios.get(`${API.url}/api/v1/donate`, {headers: headers})
    dispatch({
      type: DONATE_GET_ALL_SUCCESS,
      payload: response.data
    })
  }
  catch(err) {
    const errorMessage = handleHttpError(err)

    dispatch({
      type: DONATE_GET_ALL_FAILURE,
      payload: errorMessage
    })
  }
}
