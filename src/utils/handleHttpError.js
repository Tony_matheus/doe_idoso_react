export default (err, customMessages) => {
  console.error(err)
  console.log(err.response)

  if(err.response && err.response.data) {
    return err.response.data.error
      || (err.response.data.errors && err.response.data.errors[0])
      || (customMessages || {})[err.response.status]
      || 'Erro desconhecido'
  }
  
  return err.message || 'Erro desconhecido'
}