export default filters => {
  return Object.keys(filters || {}).reduce(
    (acc, filterField) => `${acc}&${filterField}=${filters[filterField]}`,
    ''
  )
}