export default target => {
  return target
    .split('')
    .reduce((acc, char) => isNaN(char) ? acc : acc + 1, 0)
}