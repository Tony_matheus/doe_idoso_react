import 'typeface-maven-pro'
import 'typeface-open-sans'

const dictionary = {
  'open-sans': 'Open Sans',
  'maven-pro': 'Maven Pro'
}

const resolve = props => {
  const { font = 'maven-pro' } = props
  return `font-family: ${dictionary[font]};`
}

const listNames = () => Object.keys(dictionary)

export default {
  resolve,
  listNames,
}