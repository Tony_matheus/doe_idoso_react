function fitDimensions(container, target) {
  const containerAspectRatio = container.width / container.height
  const targetAspectRatio = target.width / target.height

  if(targetAspectRatio > containerAspectRatio) {
    return {
      width: container.width,
      height: (container.width * target.height) / target.width
    }
  }
  
  if(targetAspectRatio < containerAspectRatio) {
    return {
      width: (container.height * target.width) / target.height,
      height: container.height
    }
  }
  
  return {
    width: container.width,
    height: container.height
  }
}

export default {
  fitDimensions
}