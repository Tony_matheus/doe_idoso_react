import React from 'react'
import styled from 'styled-components'

import { LoginForm, AuthAside } from '../../components/Auth'

const Wrapper = styled.div`
  display: flex;
`

const Login = props => (
  <Wrapper {...props}>
    <AuthAside />
    <LoginForm />
  </Wrapper>
)

export default Login
