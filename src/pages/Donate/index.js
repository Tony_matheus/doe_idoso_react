import React from 'react'
import styled from 'styled-components'

import DonateList from '../../components/Donate'
import {PageHeader, PageContent} from '../../components/Page'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`

const Donate = props => (
  <Wrapper {...props}>
    <PageHeader selected={"donate"}/>
    <PageContent>
      <DonateList />
    </PageContent>
  </Wrapper>
)

export default Donate
