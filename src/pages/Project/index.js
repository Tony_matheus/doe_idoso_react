import React from 'react'
import styled from 'styled-components'

import ProjectList from '../../components/Projects/ProjectList'
import {PageHeader, PageContent} from '../../components/Page'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`

const Project = props => (
  <Wrapper {...props}>
    <PageHeader selected={"project"}/>
    <PageContent>
      <ProjectList />
    </PageContent>
  </Wrapper>
)

export default Project
