import React from 'react'
import styled from 'styled-components'

import { SignUpForm, AuthAside } from '../../components/Auth'

const Wrapper = styled.div`
  display: flex;
`

const SignUp = props => (
  <Wrapper {...props}>
    <AuthAside />
    <SignUpForm />
  </Wrapper>
)

export default SignUp
