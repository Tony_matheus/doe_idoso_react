import React from 'react'
import styled from 'styled-components'

import InstitutionList from '../../components/Institution/InstitutionList'
import {PageHeader, PageContent} from '../../components/Page'
import { withAuthChecker } from '../../components/_Misc/Auth'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`

const Institution = props => (
  <Wrapper {...props}>
    <PageHeader selected={"institution"}/>
    <PageContent>
      <InstitutionList />
    </PageContent>
  </Wrapper>
)

export default withAuthChecker(Institution)
