import React from "react";
import styled from "styled-components";

import ArticleList from "../../components/Article/ArticleList";
import { PageHeader, PageContent } from "../../components/Page";

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;
const Article = props => (
  <Wrapper {...props}>
    <PageHeader selected={"article"} />
    <PageContent>
      <ArticleList />
    </PageContent>
  </Wrapper>
);

export default Article;
