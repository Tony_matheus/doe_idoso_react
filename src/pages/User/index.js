import React from 'react'
import styled from 'styled-components'

import Users from '../../components/Users'
import {PageHeader, PageContent} from '../../components/Page'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`

const User = props => (
  <Wrapper {...props}>
    <PageHeader selected={"users"}/>
    <PageContent>
      <Users />
    </PageContent>
  </Wrapper>
)

export default User
