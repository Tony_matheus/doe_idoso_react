import React from 'react'
import { Location } from '@reach/router'

import Checker from './Checker'

export default ({ action }) => (
  <Location>
    {({ location }) => 
      <Checker 
        location={location} 
        action={action} 
        />
    }
  </Location>
)