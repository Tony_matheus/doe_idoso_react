import React from 'react'
import PropTypes from 'prop-types'

class Checker extends React.Component {
  componentDidUpdate(prevProps) {
    const { location, action } = this.props
    
    if(action && location.pathname !== prevProps.location.pathname)
      action({ location })
  }

  render() {
    return null
  }
}

Checker.propTypes = {
  action: PropTypes.func.isRequired
}

export default Checker