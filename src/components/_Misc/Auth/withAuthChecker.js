import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { Redirect } from '@reach/router'
import styled from 'styled-components'
import ReactLoading from 'react-loading'

// import Logo from '../../_UI/Logo'
import Text from '../../_UI/Text'

import { vp } from '../../../utils'

import { checkToken } from '../../../redux/actions/user'

const Wrapper = styled.div`
  display: flex;
  height: 100vh;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`

// const StyledLogo = styled(Logo)`
//   width: ${vp.dk.vw(360)};

//   @media only screen and (max-width: 1280px) {
//     width: 240px;
//   }
// `

const LoadingWrapper = styled.div`
  display: flex;
  align-items: center;
  margin-top: ${vp.dk.vw(24)};

  @media only screen and (max-width: 1280px) {
    margin-top: 16px;
  }
`

const LoadingMessage = styled(Text).attrs(props => ({
  font: 'open-sans'
}))`
  font-size: ${vp.dk.vw(33)};
  margin-left: ${vp.dk.vw(12)};
  color: #505050;

  @media only screen and (max-width: 1280px) {
    font-size: 22px;
    margin-left: 8px;
  }
`

const Loading = styled(ReactLoading).attrs({
  className: 'loading'
})`
  width: ${vp.dk.vw(24)} !important;
  height: ${vp.dk.vw(24)} !important;

  @media only screen and (max-width: 1280px) {
    width: 16px !important;
    height: 16px !important;  
  }
`

const withConnect = Component => {
  const mapStateToProps = state => ({ 
    isAuthenticated: state.user.isAuthenticated,
    didFirstCheck: state.user.didFirstCheck
  })
  const actions = { checkToken }
  
  return connect(mapStateToProps, actions)(Component)
}

const withAuthChecker = Component => withConnect(props => {
  const { isAuthenticated, didFirstCheck, checkToken, ...restProps } = props

  useEffect(
    () => { checkToken() }, 
    [checkToken]
  )
  
  if(!didFirstCheck) {
    return (
      <Wrapper>
        {/* <StyledLogo /> */}
        <LoadingWrapper>
          <Loading 
            type="spin"
            color="black"
            />
          <LoadingMessage>Aguarde...</LoadingMessage>
        </LoadingWrapper>
      </Wrapper>
    )
  }

  return (
    isAuthenticated
      ? <Component {...restProps} />
      : <Redirect
          noThrow
          to="/"
          />
  )
})

export default withAuthChecker