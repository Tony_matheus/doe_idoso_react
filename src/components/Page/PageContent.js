import styled from 'styled-components'

import { vp } from '../../utils'

const PageContent = styled.div`
  width: ${vp.dk.vw(1410)};
  margin-top: ${vp.dk.vw(28)};

  @media only screen and (max-width: 1280px) {
    width: 940px;
    margin-top: 18px;
  }
`
PageContent.displayName = 'PageContent'

export default PageContent