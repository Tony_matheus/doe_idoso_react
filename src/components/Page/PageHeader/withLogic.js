import React, { useState, useEffect } from 'react';
import { navigate } from '@reach/router';
import { useSelector, connect } from 'react-redux';
import { logout } from '../../../redux/actions/user';

const withConnect = Component => {
  const actions = { logout}

  return connect(
    null,
    actions
  )(Component)
}

export default Component => withConnect(props => {
  const logout = ( ) => {
    props.logout()
    navigate("/")
  }
  
  return (
    <Component
      {...props}
      logout={logout}
    />
  )
})

