import React from 'react'
import styled from 'styled-components'
import { Redirect, navigate } from '@reach/router'
import { Button } from 'antd'
import AccountMenu from '../AccountMenu'
import withLogic from './withLogic'
import { Link } from '../../_UI/Button'

import { vp } from "../../../utils";

const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  padding: auto ${vp.dk.vw(10)};
  box-sizing: border-box;
  position: relative;
  z-index: 10;
  width: 100%;
  background: #fff;
  -webkit-box-shadow: 0 2px 8px #f0f1f2;
  box-shadow: 0 2px 8px #f0f1f2;
  
  @media only screen and (max-width: 1280px){
    padding: 10px 6px;
  }
`

const LinksWrapper = styled.div`
  display: inline-flex;
  margin-left: ${vp.dk.vw(30)};
  
  @media only screen and (max-width: 1280px){
    margin-left: 20px;
  }
`

const LinksColumn = styled.div`
  flex-grow: 1;
  padding: ${vp.dk.vw(20)};
  
  ${props => props.selected && `
    border-bottom: 1px solid green;
    a {
      color: green;
    }
    
  `}
  
  &:hover{
    border-color: green;
    color: green;
  }
  
  @media only screen and (max-width: 1280px){
    padding: auto 13px;
  }
`

const LinkRedirect = styled(Link)`
  font-size: ${vp.dk.vw(20)};
  font-weight: 400;
  text-decoration: none;
  
  &:hover{
    color: green;
  }
  
  @media only screen and (max-width: 1280px){
    font-size: 13px;
  }
`

const ButtonLogout = styled(Button)`

`

const PageHeader = (props) => {
  const {
    isAccountMenuVisible,
    accountMenuOptions,
    onAccountMenuOptionClick,
    selected,
    logout,
    ...restProps
  } = props

  return (
    <Wrapper {...restProps}>
      <LinksWrapper>
        <LinksColumn selected={selected === 'project'} onClick={() => navigate("/project")}>
          <LinkRedirect>
            Projetos
          </LinkRedirect>
        </LinksColumn>
        <LinksColumn selected={selected === 'institution'} onClick={() => navigate("/institution")}>
          <LinkRedirect>
            Instituição
          </LinkRedirect>
        </LinksColumn>
        <LinksColumn selected={selected === 'article'} onClick={() => navigate("/article")}>
          <LinkRedirect>
            Notícias
          </LinkRedirect>
        </LinksColumn>
        <LinksColumn selected={selected === 'donate'} onClick={() => navigate("/donate")}>
          <LinkRedirect>
            Doação
          </LinkRedirect>
        </LinksColumn>
        <LinksColumn selected={selected === 'users'} onClick={() => navigate("/users")}>
          <LinkRedirect>
            Usuários
          </LinkRedirect>
        </LinksColumn>
        <LinksColumn style={{ cursor: "pointer" }} selected={selected === 'logout'} onClick={logout}>
          <LinkRedirect>
            Deslogar
          </LinkRedirect>
        </LinksColumn>
      </LinksWrapper>

      <AccountMenu
        isVisible={isAccountMenuVisible}
        options={accountMenuOptions}
        onOptionClick={onAccountMenuOptionClick} 
                                                                                           c/>
    </Wrapper>
  )
}

export default withLogic(PageHeader)
