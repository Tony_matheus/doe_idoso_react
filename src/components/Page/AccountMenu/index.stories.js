import React from 'react'
import { storiesOf } from '@storybook/react'
import { withKnobs, array } from '@storybook/addon-knobs'
import { action } from '@storybook/addon-actions'

import AccountMenu from './'

const options = ['Option 1', 'Option 2', 'Option 3']

storiesOf('Page/AccountMenu', module)
  .addDecorator(withKnobs)
  .add('default', () => 
    <div style={{ paddingTop: '20px' }}>
      <AccountMenu
        options={array('options', options)}
        onOptionClick={action('onOptionClick')}
        />
    </div>
  )