import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import posed from 'react-pose'

import { Divider } from '../../_UI/Divider'
import { Link } from '../../_UI/Button'

import { vp } from '../../../utils'

const Wrapper = posed(styled.div`
  display: flex;
  flex-direction: column;
  width: ${vp.dk.vw(218)};
  align-items: center;
  background: black;

  @media only screen and (max-width: 1280px) {
    width: 145px;
  }
`)({
  visible: { 
    applyAtStart: { display: 'flex' },
    opacity: 1,
    transition: { duration: 300 }
  },
  hidden: {
    applyAtEnd: { display: 'none' },
    opacity: 0, 
    transition: { duration: 200 }
  },
})

const OptionWrapper = styled.div`
  width: 95%;
  display: flex;
  flex-direction: column;
  align-items: center;
`

const StyledDivider = styled(Divider)`
  width: 100%;
  background: white;
`
const Option = styled(Link)`
  color: white;
  padding: ${vp.dk.vw(6)} 0px;
  font-size: ${vp.dk.vw(18)};
  text-transform: lowercase;
  font-weight: 500;

  @media only screen and (max-width: 1280px) {
    padding: 4px 0px;
    font-size: 12px;
  }
`

const AccountMenu = ({ options, onOptionClick, isVisible, ...restProps }) => (
  <Wrapper 
    {...restProps}
    pose={isVisible ? 'visible' : 'hidden'}>
    {options.map((option, optionIndex) => 
      <OptionWrapper
        key={optionIndex}
        onClick={() => onOptionClick(optionIndex)}>
        {optionIndex !== 0 && <StyledDivider />}
        <Option>{option}</Option>
      </OptionWrapper>
    )}
  </Wrapper>
)

AccountMenu.defaultProps = {
  options: [],
  onOptionClick: () => console.log('Not implemented')
}

AccountMenu.propTypes = {
  options: PropTypes.arrayOf(PropTypes.string).isRequired,
  onOptionClick: PropTypes.func.isRequired,
  isVisible: PropTypes.bool.isRequired
}

export default AccountMenu