import PageHeader from './PageHeader'
import PageContent from './PageContent'

export{
  PageHeader,
  PageContent
}
