import React from 'react'
import {Divider} from 'antd';
import {connect} from 'react-redux'

import {getAll} from '../../redux/actions/donate'

const withConnect = Component => {
  const mapStateToProps = state => ({
    donate: state.donate,
    isGettingAll: state.donate.isGettingAll,
    isCreating: state.donate.isCreating,
    isDeleting: state.donate.isDeleting,
  })

  const actions = {
    getContent: getAll
  }
  return connect(mapStateToProps, actions)(Component)
}

const withLogic = Component => withConnect(class extends React.Component {
    constructor(props) {
      super(props)

      this.alertBarRef = React.createRef()

      this.state = {
        columns: [
          {
            title: 'Instituição',
            dataIndex: 'name',
            key: 'name',
          },
          {
            title: 'Descrição',
            dataIndex: 'description',
            key: 'description'
          },
          {
            title: 'Action',
            key: 'action',
            render: (text, record) => (
              <span>
              {/* <a href="#" onClick={() => this.handleOpenModal(record.id)}>Editar </a> */}
                <Divider type="vertical"/>
              {/* <a href="#" onClick={() => this.handleDeleteDonate(record.id)}>Deletar</a> */}
            </span>
            ),
          },
        ],
        data: Array(18).fill(1).map((_, index) => ({
          key: index,
          id: index,
          name: 'John Brown' + index,
          age: 32,
          description: 'New York No. 1 Lake Park',
          tags: ['nice', 'developer'],
        })),
        formModal: {
          formFields: [],
          formValues: {},
          isModalVisible: true,
          isMultiple: true,
        }
      }
    }

    render() {
      return (
        <Component
          columns={this.state.columns}
          data={this.props.donate.all}
          isLoading={this.props.isGettingAll || this.props.isCreating || this.props.isDeleting }
          isFormMultiple={this.state.isMultiple}
          onPositiveButtonClick={this.handlePositiveButtonClick}
          onNegativeButtonClick={this.handleNegativeButtonClick}
          onSelectOptionClick={this.handleSelectionOptionClick}
        />
      )
    }

    componentDidMount() {
      this.props.getContent()
    }

    componentDidUpdate(prevProps) {
    }

    handlePositiveButtonClick = () => {
      console.log(this.state.formModal.formValuesModal)
      this.props.create(this.state.formModal.formValuesModal)
      this.setState(state => ({
        ...state,
        isModalVisible: false
      }))
    }

    handleNegativeButtonClick = () => {
      this.setState(state => ({
        ...state,
        isModalVisible: false
      }))
    }

    handleSelectionOptionClick = ({index, name}) => {
      this.setState(state => ({
        formValues: {
          ...state.formValues,
          [name]: index
        }
      }))
    }
  }
)

export default withLogic
