import React from 'react'
import { storiesOf } from '@storybook/react'

import InstitutionList from './'

const columns = [
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
    render: text => <a href="javascript:;">{text}</a>,
  },
  {
    title: 'Age',
    dataIndex: 'age',
    key: 'age',
  },
  {
    title: 'Address',
    dataIndex: 'address',
    key: 'address',
  },
  {
    title: 'Tags',
    key: 'tags',
    dataIndex: 'tags',
    render: tags => (
      <span>
        {tags.map(tag => {
          let color = tag.length > 5 ? 'geekblue' : 'green';
          if (tag === 'loser') {
            color = 'volcano';
          }
          return (
            <span>oi</span>
          );
        })}
      </span>
    ),
  },
  {
    title: 'Action',
    key: 'action',
    render: (text, record) => (
      <span>
       oi
      </span>
    ),
  },
];
const data = Array(20).fill(1).map((_, index) => ({
  key: '1',
  name: 'John Brown',
  age: 32,
  address: 'New York No. 1 Lake Park',
  tags: ['nice', 'developer'],
}))

storiesOf('Auth|Institution/Institution List', module)
  .add('default', () =>
    <div style={{ paddingLeft: '50px' }}>
      <InstitutionList columns={columns} data={data}/>
    </div>
  )
