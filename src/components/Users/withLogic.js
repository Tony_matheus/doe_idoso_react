import React from 'react'
import { connect } from "react-redux";
import { message, Divider } from 'antd'
import { getAll, register, deleteUser } from "../../redux/actions/user";


const withConnect = Component => {
  const mapStateToProps = state => ({
    user: state.user,
    isGettingAll: state.user.isGettingAll,
    isCreating: state.user.isCreating,
    isDeleting: state.user.isDeleting
  });

  const actions = {
    getContent: getAll,
    register,
    deleteUser
  };
  return connect(
    mapStateToProps,
    actions
  )(Component);
};

const withLogic = Component => withConnect(class extends React.Component {
  constructor(props) {
    super(props)

    this.alertBarRef = React.createRef();

    this.state = {
      columns: [
        {
          title: "Nome",
          dataIndex: "name",
          key: "name"
        },
        {
          title: "Email",
          dataIndex: "email",
          key: "email"
        },
        {
          title: "Action",
          key: "action",
          render: (text, record) => (
            <span>
              {/* <a href="#" onClick={() => this.handleOpenModal(record.id)}>
                Editar{" "}
              </a>
              <Divider type="vertical" /> */}
              <a
                href="#"
                onClick={() => this.handleDeleteUser(record.id)}
              >
                Deletar
              </a>
            </span>
          )
        }
      ],
      data: Array(18)
        .fill(1)
        .map((_, index) => ({
          key: index,
          id: index,
          name: "John Brown" + index,
          age: 32,
          email: "New York No. 1 Lake Park",
        })),
      formModal: {
        formFields: [],
        formValues: {},
        isModalVisible: true,
        isMultiple: true
      }
    }
  }

  render() {
    return (
      <Component
        columns={this.state.columns}
        data={this.props.user.all}
        isLoading={this.props.user.isGettingAll}
        isFormMultiple={this.state.isMultiple}
        modalTitle={this.state.formModal.title}
        isModalVisible={this.state.isModalVisible}
        formModal={this.state.formModal}
        onPositiveButtonClick={this.handlePositiveButtonClick}
        onNegativeButtonClick={this.handleNegativeButtonClick}
        onSelectOptionClick={this.handleSelectionOptionClick}
        onFieldChangeModal={this.handleFieldChangeModal}
        onOpenModal={this.handleOpenModal}
      />
    )
  }
  componentDidMount() {
    this.props.getContent();
  }
  componentDidUpdate(prevProps) {
    if (prevProps.user.isCreating && !this.props.user.isCreating) {
      if (!prevProps.user.error && this.props.user.error)
        return message.error('Falha ao Criar usuário');
      console.log(this.props.user.isCreating)
      return message.success('Usuário Criado com sucesso');
    }


    if (prevProps.user.isDeleting && !this.props.user.isDeleting) {
      if (!prevProps.user.error && this.props.user.error)
        return message.error('Falha ao Deletar usuário');
      console.log(this.props.user.isDeleting)
      return message.success('Usuário Criado com sucesso');
    }
  }

  handleOpenModal = (id = null) => {
    console.log(id);
    this.setState(state => ({
      isModalVisible: true,
      isMultiple: true,
      formModal: {
        ...state.formModal,
        formFields: [
          {
            title: "Novo Usuário",
            fields: [
              {
                name: "name",
                placeholder: "Nome"
              },
              {
                name: "email",
                placeholder: "email"
              },
              {
                name: "password",
                placeholder: "Senha",
                type: "password"
              },
              {
                name: "password_confirmation",
                placeholder: "Confirmação de Senha",
                type: "password"
              }
            ]
          }
        ],
        formValuesModal:
          id !== null
            ? this.handleEditArticle(id)
            : {
              name: "12345678",
              email: "ma@ma.com",
              password: "12345678",
              password_confirmation: "12345678"
            },
        title: id ? "Editando Usuário: " : "Criando Usuário"
      }
    }));
  };

  handleFieldChangeModal = event => {
    const fieldName = event.target.name;
    const fieldValue = event.target.value;

    this.setState(state => ({
      formModal: {
        ...state.formModal,
        formValuesModal: {
          ...state.formModal.formValuesModal,
          [fieldName]: fieldValue
        }
      }
    }));
  };

  handlePositiveButtonClick = () => {
    console.log(this.state.formModal.formValuesModal);
    this.props.register(this.state.formModal.formValuesModal);
    this.setState(state => ({
      ...state,
      // isModalVisible: false
    }));
  };

  handleNegativeButtonClick = () => {
    this.setState(state => ({
      ...state,
      isModalVisible: false
    }));
  };

  handleEditUser = userId =>
    this.props.user.all.find(user => user.id === userId);

  handleDeleteUser = userId => {
    this.props.deleteUser(userId);
  };

  handleSelectionOptionClick = ({ index, name }) => {
    this.setState(state => ({
      formValues: {
        ...state.formValues,
        [name]: index
      }
    }));
  };
})

export default withLogic