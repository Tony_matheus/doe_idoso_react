import React from 'react';
import styled from 'styled-components'
import ListCrud from "../_UI/ListCrud";
import withLogic  from './withLogic'

const Users  = (props) => {
  return <ListCrud {...props} />;
}

export default withLogic(Users)