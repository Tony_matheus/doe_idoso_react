import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import posed from 'react-pose'

import Text from '../../Text'
import {LabelDivider} from '../../Divider'
import {Button, BorderButton} from '../../Button'
import Select from "../../Input/Select";
import TextBox from "../../Input/TextBox";

import {vp} from '../../../../utils'

const Wrapper = posed(styled.div`
  position: fixed;
  z-index: 9999;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  display: flex;
  justify-content: center;
  align-items: center;
`)({
  visible: {
    applyAtStart: {display: 'flex'},
    opacity: 1,
    transition: {duration: 200}
  },
  hidden: {
    applyAtEnd: {display: 'none'},
    opacity: 0,
    transition: {duration: 200}
  },
})

const Backdrop = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;  
  background: black;
  opacity: 0.5;
`

const Card = styled.div`
  width: ${vp.dk.vw(800)};
  background: white;
  border-radius: 5px;
  display: inline-flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  z-index: 1;
  padding: ${vp.dk.vw(42)} ${vp.dk.vw(30)};

  @media only screen and (max-width: 1280px) {
    width: 533px;
    padding: 28px 20px;
  }
`

// const Message = styled(Text).attrs({font: 'open-sans'})`
//   color: black;
//   font-size: ${vp.dk.vw(21)};
//   text-align: center;
//   margin-bottom: ${vp.dk.vw(42)};
//   font-weight: 600;
//
//   @media only screen and (max-width: 1280px) {
//     font-size: 14px;
//     margin-bottom: 28px;
//   }
// `

const FormContainer = styled.div`
  display: flex;
  width: 100%;
  max-height: 650px;
  overflow-y: auto;
  flex-direction: column;
`

const FormWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-self: center;
  padding: ${vp.dk.vw(60)} ${vp.dk.vw(45)};
  
  
  @media only screen and (max-width: 1280px){
    padding: 40px 30px ;
  }
`

const Column = styled.div`
  display: inline-flex;
  flex-direction: column;
`

const ButtonColumn = styled(Column)`
  padding-right: ${vp.dk.vw(10)}; 
  margin-top: ${vp.dk.vw(20)};
  flex-grow: 1;
  
  @media only screen and (max-width: 1280px){ 
    margin-top: 13px;
    padding-right: 6px;
  }
`

const TitleFormPart = styled(LabelDivider)`
  p {
    font-weight: 700;
  }
  margin-bottom: 15px;
  //text-transform: bold;
`

const LabelTextBox = styled(Text)`
  color: black;
  font-size: ${vp.dk.vw(15)};
  font-weight: 400;
  text-transform: uppercase;
  letter-spacing: 2.72px;
  margin-left: ${vp.dk.vw(3)};
  margin-bottom: ${vp.dk.vw(7)};

  @media only screen and (max-width: 1280px) {
    font-size: 10px;
    letter-spacing: 1.81px;
    margin-left: 2px;
    margin-bottom: 4px;
  }
`

const FieldTextBox = styled(TextBox)`
  border-color: #e1e1e1;
  background-color: white;
  width: ${vp.dk.vw(600)};
  height: ${vp.dk.vw(39)};
  
  input {
    width: ${vp.dk.vw(560)};
  }

  @media only screen and (max-width: 1280px){
    width: 400px;
    height: 26px;

    input {
      width: 373px;
    }
  }  
`
//
// const Footer = styled.div`
//   display: flex;
//   justify-content: center;
//   align-items: center;
// `

const ButtonsWrapper = styled.div`
  margin-top: ${vp.dk.vw(20)};
  display: inline-flex;
  width: 55%;
  justify-content: space-between;
  
  @media only screen and (max-width: 1280px){
    margin-top: 13px;
  }
  
`

const NegativeButton = styled(BorderButton)`
  min-width: 94px;
  height: ${vp.dk.vw(42)};

  @media only screen and (max-width: 1280px) {
    height: 28px;
  }
`

const PositiveButton = styled(Button)`
  min-width: 94px;
  height: ${vp.dk.vw(42)};

  @media only screen and (max-width: 1280px) {
    height: 28px;
  }
`

const TitleSection = styled.div`
  padding-top: ${vp.dk.vw(20)};
  
  @media only screen and (max-width: 1280px){
    padding-top: 13px;
  }
`

const FormModal = props => {
  const {
    positiveButtonLabel,
    negativeButtonLabel,
    onNegativeButtonClick,
    onPositiveButtonClick,
    message,
    title,
    isVisible,
    fields,
    values,
    formValues,
    onSelectOptionClick,
    onFieldChange,
    alertBarRef,
    isFormMultiple,
    isLoading,
    ...restProps
  } = props

  console.log(isFormMultiple)
  const currentValues = values || {}
  return (
    <Wrapper
      {...restProps}
      pose={isVisible ? 'visible' : 'hidden'}>
      <Backdrop onClick={onNegativeButtonClick}/>
      <Card>
        <LabelDivider children={title || 'No message'}/>
        <FormContainer>
          {!isFormMultiple
            ? <FormWrapper>
              {fields.map((field, fieldIndex) =>
                field.component === 'select'
                  ? <ButtonColumn key={fieldIndex}>
                    <Select
                      name={field.name}
                      placeholder={field.placeholder}
                      data={field.data}
                      selectedIndex={formValues[field.name]}
                      onOptionClick={onSelectOptionClick}
                    />
                  </ButtonColumn>
                  : <ButtonColumn key={fieldIndex}>
                    <LabelTextBox>{field.placeholder}</LabelTextBox>
                    <FieldTextBox
                      name={field.name}
                      placeholder={field.placeholder}
                      onChange={onFieldChange}
                      mask={field.mask}
                      value={currentValues[field.name] ? currentValues[field.name] : ""}
                      type={field.type ? field.type : "text"}
                    />
                  </ButtonColumn>
              )}
            </FormWrapper>
            : <FormWrapper>
              {fields.map((section, sectionIndex) => (
                <React.Fragment key={sectionIndex}>
                  {sectionIndex >= 1
                    ? <TitleSection>
                      <TitleFormPart children={section.title}/>
                    </TitleSection>
                    : <TitleFormPart children={section.title}/>}
                  {section.fields.map((field, fieldIndex) =>
                    field.component === 'select'
                      ? <ButtonColumn key={fieldIndex}>
                        <Select
                          name={field.name}
                          placeholder={field.placeholder}
                          data={field.data}
                          selectedIndex={formValues[field.name]}
                          onOptionClick={onSelectOptionClick}
                        />
                      </ButtonColumn>
                      : <ButtonColumn key={fieldIndex}>
                        <LabelTextBox>{field.placeholder}</LabelTextBox>
                        <FieldTextBox
                          name={field.name}
                          placeholder={field.placeholder}
                          onChange={onFieldChange}
                          mask={field.mask}
                          value={currentValues[field.name] ? currentValues[field.name] : ""}
                          type={field.type ? field.type : "text"}
                        />
                      </ButtonColumn>
                  )}
                </React.Fragment>
              ))}
            </FormWrapper>
          }
        </FormContainer>
        {/*<Message>{message || 'No message'}</Message>*/}
        <ButtonsWrapper>
          <NegativeButton onClick={onNegativeButtonClick}>
            {negativeButtonLabel || 'NO'}
          </NegativeButton>
          <PositiveButton onClick={onPositiveButtonClick} isLoading={isLoading}>
            {positiveButtonLabel || 'YES'}
          </PositiveButton>
        </ButtonsWrapper>
      </Card>
    </Wrapper>
  )
}

FormModal.propTypes = {
  positiveButtonLabel: PropTypes.string.isRequired,
  negativeButtonLabel: PropTypes.string.isRequired,
  onNegativeButtonClick: PropTypes.func.isRequired,
  onPositiveButtonClick: PropTypes.func.isRequired,
  message: PropTypes.string,
  isVisible: PropTypes.bool.isRequired
}

export default FormModal
