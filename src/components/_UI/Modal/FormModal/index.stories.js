import React from 'react'
import { storiesOf } from '@storybook/react'
import { withKnobs, text, boolean } from '@storybook/addon-knobs'
import { action } from '@storybook/addon-actions'

import FormModal from './'

const fields = [
  {
    name: 'id',
    placeholder: 'ID'
  },
  {
    name: 'startDate',
    placeholder: 'Data inicial',
    mask: '99/99/9999'
  },
  {
    name: 'endDate',
    placeholder: 'Data final',
    mask: '99/99/9999'
  },
  {
    name: 'status',
    placeholder: '- Todos os status -',
  },
  {
    name: 'subsidiary',
    placeholder: 'Filial'
  },
]

storiesOf('UI|Modal/FormModal', module)
  .addDecorator(withKnobs)
  .add('default', () => 
    <div style={{ paddingTop: '20px' }}>
      <FormModal
        message={text('message', 'Some message here')}
        buttonLabel={text('buttonLabel', 'Nice!')}
        isVisible={boolean('isVisible', true)}
        onButtonClick={action('onButtonClick')}
        fields={fields}
        />
    </div>
  )
