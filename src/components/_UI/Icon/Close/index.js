import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

import { vp } from '../../../../utils'

const Wrapper = styled.div`
  width: ${vp.dk.vw(18)};
  height: ${vp.dk.vw(18)};
  //border: 1px solid red;
  position: relative;

  @media only screen and (max-width: 1280px) {
    width: 12px;
    height: 12px;
  }
`

const Edge = styled.div`
  width: 100%;
  height: 2px;
  background: #979596;
  position: absolute;
  top: 50%;
  transform: translateY(-25%) rotate(${props => props.right ? -45 : 45}deg);
`

// const Icon = styled.div`
//   border-bottom: 2px solid ${props => props.colour || '#979596'};
//   border-right: 2px solid ${props => props.colour || '#979596'};
//   width: ${vp.dk.vw(5)};
//   height: ${vp.dk.vw(10)};
//   position: absolute;
//   transform: translate(-40%, -69%) rotate(45deg);
//   top: 50%;
//   left: 50%;
//   opacity: 1;
//   transition: opacity .3s;
//   z-index: 1;

//   @media only screen and (max-width: 1280px) {
//     width: 3px;
//     height: 6px;
//   }
// `

const Close = ({ colour, ...restProps }) => (
  <Wrapper {...restProps}>
    <Edge />
    <Edge right/>
  </Wrapper>
)

Close.propTypes = {
  colour: PropTypes.string
}

export default Close