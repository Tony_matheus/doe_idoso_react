import React from 'react'
import { storiesOf } from '@storybook/react'
import { withKnobs, text } from '@storybook/addon-knobs'

import Close from './'

storiesOf('UI|Icon/Close', module)
  .addDecorator(withKnobs)
  .add('default', () => (
    <div style={{ padding: '5px' }}>
      <Close />
    </div>
  ))
  .add('with colour', () => (
    <div style={{ padding: '5px' }}>
      <Close colour={text('colour', 'tomato')} />
    </div>
  ))