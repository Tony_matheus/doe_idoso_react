import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

const Wrapper = styled.div`
  display: inline-flex;
  width: 10px;
  height: 10px;
  //border: 1px solid red;
`

const Icon = styled.div`
  border-bottom: 2px solid ${props => props.color || '#979596'};
  border-right: 2px solid ${props => props.color || '#979596'};
  //border: 2px solid #979596;
  box-sizing: border-box;
  width: 100%;
  height: 100%;
  //transform: rotate(225deg);
  opacity: 1;
  transition: opacity .3s;
  z-index: 1;

  ${props => !props.isVisible && 'opacity: 0;'}
  // ${props => props.down && 'transform:  rotate(45deg);'}
  // ${props => props.left && 'transform: translateX(25%) rotate(135deg);'}
  // ${props => props.right && 'transform translateX(-25%): rotate(-45deg);'}

  //button > & {
    transform: translateY(25%) rotate(225deg);

    ${props => props.down && 'transform: translateY(-25%) rotate(45deg);'}
    ${props => props.left && 'transform: translateX(25%) rotate(135deg);'}
    ${props => props.right && 'transform: translateX(-25%) rotate(-45deg);'}
  //}
`

const Arrow = ({ isVisible, left, right, down, color, ...restProps }) => (
  <Wrapper {...restProps}>
    <Icon 
      isVisible={isVisible}
      left={left}
      right={right}
      down={down}
      color={color}
      />
  </Wrapper>
)

Arrow.defaultProps = {
  isVisible: true
}

Arrow.propTypes = {
  isVisible: PropTypes.bool,
  left: PropTypes.bool,
  right: PropTypes.bool,
  down: PropTypes.bool,
}

export default Arrow