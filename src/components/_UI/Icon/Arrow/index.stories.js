import React from 'react'
import { storiesOf } from '@storybook/react'
import { withKnobs } from '@storybook/addon-knobs'

import Arrow from './'

storiesOf('UI|Icon/Arrow', module)
  .addDecorator(withKnobs)
  .add('up (default)', () => (
    <div style={{ padding: '5px' }}>
      <Arrow />
    </div>
  ))
  .add('down', () => (
    <div style={{ padding: '5px' }}>
      <Arrow down />
    </div>
  ))
  .add('left', () => (
    <div style={{ padding: '5px' }}>
      <Arrow left />
    </div>
  ))
  .add('right', () => (
    <div style={{ padding: '5px' }}>
      <Arrow right />
    </div>
  ))