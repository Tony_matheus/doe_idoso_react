import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

import { vp } from '../../../../utils'

const Wrapper = styled.div`
  width: ${vp.dk.vw(10)};
  height: ${vp.dk.vw(10)};
  //border: 1px solid red;
  position: relative;

  @media only screen and (max-width: 1280px) {
    width: 6px;
    height: 6px;
  }
`

const Icon = styled.div`
  border-bottom: 2px solid ${props => props.colour || '#979596'};
  border-right: 2px solid ${props => props.colour || '#979596'};
  width: ${vp.dk.vw(5)};
  height: ${vp.dk.vw(10)};
  position: absolute;
  transform: translate(-40%, -69%) rotate(45deg);
  top: 50%;
  left: 50%;
  opacity: 1;
  transition: opacity .3s;
  z-index: 1;

  @media only screen and (max-width: 1280px) {
    width: 3px;
    height: 6px;
  }
`

const Checked = ({ colour, ...restProps }) => (
  <Wrapper {...restProps}>
    <Icon colour={colour} />
  </Wrapper>
)

Checked.propTypes = {
  colour: PropTypes.string
}

export default Checked