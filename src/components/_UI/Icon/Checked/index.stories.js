import React from 'react'
import { storiesOf } from '@storybook/react'
import { withKnobs, text } from '@storybook/addon-knobs'

import Checked from './'

storiesOf('UI|Icon/Checked', module)
  .addDecorator(withKnobs)
  .add('default', () => (
    <div style={{ padding: '5px' }}>
      <Checked />
    </div>
  ))
  .add('with colour', () => (
    <div style={{ padding: '5px' }}>
      <Checked colour={text('colour', 'tomato')} />
    </div>
  ))