import React from 'react'
import { storiesOf } from '@storybook/react'
import { withKnobs, select } from '@storybook/addon-knobs'

import Icon from './'

import resolver from './resolver'

const options = Object.keys(resolver)
const defaultOption = options[0]

storiesOf('UI|Icon/Icon', module)
  .addDecorator(withKnobs)
  .add('default', () => (
    <div style={{ padding: '5px' }}>
      <Icon name={select('name', options, defaultOption)} />
    </div>
  ))
