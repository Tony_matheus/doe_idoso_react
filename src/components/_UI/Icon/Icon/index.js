import React from 'react'

import resolver from './resolver'

const Icon = ({ name, ...restProps }) => {
  const iconSrc = resolver[name || 'facebook']

  return (
    <img
      {...restProps}
      alt="social media icon"
      src={iconSrc}/>
  )
}

export default Icon
