import facebookIcon from './images/facebook-icon.png'
import instagramIcon from './images/instagram-icon.png'
import linkedInIcon from './images/linkedin-icon.jpg'
import youtubeIcon from './images/youtube-icon.png'
import trashIcon from './images/trash-icon.png'
import plusIcon from './images/plus-icon.svg'
import minusIcon from './images/minus-icon.svg'
import editIconBlack from './images/edit-icon-black.png'
import pdfIcon from './images/pdf-icon.svg'
import addIcon from './images/add-icon.png'
import editIcon from './images/edit-icon.png'
import saveIcon from './images/save-icon.png'
import statusCheckedIcon from './images/status-checked-icon.png'

const resolver = {
  facebook: facebookIcon,
  instagram: instagramIcon,
  linkedin: linkedInIcon,
  youtube: youtubeIcon,
  trash: trashIcon,
  plus: plusIcon,
  minus: minusIcon,
  editBlack: editIconBlack,
  pdf: pdfIcon,
  add: addIcon,
  edit: editIcon,
  save: saveIcon,
  statusChecked: statusCheckedIcon
}

export default resolver
