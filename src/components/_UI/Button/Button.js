import React from 'react'
import styled, { css } from 'styled-components'
import PropTypes from 'prop-types'
import ReactLoading from 'react-loading'

import { fontDictionary, vp } from '../../../utils'

const RealButton = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  min-width: 90px;
  height: ${vp.dk.vw(37)};
  padding: 0 ${vp.dk.vw(15)};
  text-decoration: none;
  border: 1px solid transparent;
  outline: none;
  background: black;
  color: white;
  font-size: ${vp.dk.vw(16)};
  font-weight: 500;
  line-height: ${vp.dk.vw(24)};
  letter-spacing: 1px;
  text-transform: uppercase;
  border-radius: 3px;
  //box-sizing: border-box;
  transition: opacity .1s, color .1s, background .1s, background .1s, border .1s, width .3s, min-width .3s;

  &:hover {
    opacity: 0.8;
  }

  &:hover:disabled {
    opacity: 1;
  }

  &:active {
    opacity: 1;
    background: white;
    color: black;
    border: 1px solid black;
  }

  &:disabled {
    background: #919191;
  }

  ${props => props.white && `
    background: white;
    color: black;
    
    &:active {
      background: black;
      color: white;
      border: 1px solid white;
    }
  `}

  ${props => props.secondary && css`
    background: #f6a4a8;
    
    &:active {
      background: transparent;
      color: #f6a4a8;
      border: 1px solid #f6a4a8;
    }
  `}

  ${props => props.isloading && css`
    &:disabled {
      background: ${props => {
        if(props.white)
          return 'white'
        
        if(props.secondary)
          return '#f6a4a8'
        
        return 'black'
      }};
    }

    & .loading {
      fill: ${props => props.white ? 'black' : 'white'} !important;
    }
  `}

  @media only screen and (max-width: 1280px) {
    min-width: 60px;
    height: 24px;
    font-size: 10px;
    line-height: 16px;
  }
`

const Loading = styled(ReactLoading).attrs({
  className: 'loading'
})`
  width: ${vp.dk.vw(24)} !important;
  height: ${vp.dk.vw(24)} !important;

  @media only screen and (max-width: 1280px) {
    width: 16px !important;
    height: 16px !important;  
  }
`

const Button = ({ isLoading, disabled, children, ...restProps }) => {
  const currentChild = isLoading 
    ? <Loading 
        type="spin"
        color={'white'} 
        />
    : children
  
  return (
    <RealButton 
      {...restProps}
      isLoading={isLoading}
      disabled={isLoading || disabled}>
      {currentChild}
    </RealButton>
  )
}

Button.defaultProps = {
  children: 'Botão'
}

Button.propTypes = {
  font: PropTypes.string,
  disabled: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.number,
  ]),
  onClick: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired,
  isLoading: PropTypes.bool
}

export default Button
