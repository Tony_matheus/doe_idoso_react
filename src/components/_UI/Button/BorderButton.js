import styled from 'styled-components'

import Button from './Button'

const BorderButton = styled(Button)`
  background-color: transparent;
  color: ${props => props.white ? 'white' : 'black'};
  border: 1px solid ${props => props.white ? 'white' : 'black'};
  
  &:hover {
    opacity: .6;
  }
  
  &:active {
    opacity: 1;
    background-color: ${props => props.white ? 'white' : 'black'};
    color: ${props => props.white ? 'black' : 'white'};
    border: 1px solid ${props => props.white ? 'white' : 'black'};;
  }
  
  &:disabled {
    color: #919191;
    border-color: #919191;
    background-color: transparent;
  }  
`

export default BorderButton
