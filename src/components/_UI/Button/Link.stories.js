import React from 'react'
import { storiesOf } from '@storybook/react'
import { withKnobs, boolean } from '@storybook/addon-knobs'

import Link from './Link'

storiesOf('UI|Button/Link', module)
  .addDecorator(withKnobs)
  .add('default', () =>
      <div style={{ padding: '5px' }}>
          <Link 
            disabled={boolean('disabled', false)}
            href="https://www.google.com"
            target="_blank">
            Some Link
          </Link>
      </div>
  )
  .add('disabled', () =>
      <div style={{ padding: '5px' }}>
          <Link 
            disabled={boolean('disabled', true)}
            href="https://www.google.com"
            target="_blank">
            Some Link
          </Link>
      </div>
  )
