import React from 'react'
import { storiesOf } from '@storybook/react'
import { withKnobs, text, select, boolean } from '@storybook/addon-knobs'
import { action } from '@storybook/addon-actions';

import WideButton from './WideButton'

import { fontDictionary } from '../../../utils'

storiesOf('UI|Button/WideButton', module)
  .addDecorator(withKnobs)
  .add('default', () => 
    <WideButton
      secondary={boolean('secondary', false)}
      white={boolean('white', false)}
      disabled={boolean('disabled', false)}
      isLoading={boolean('isLoading', false)}
      font={select('font', fontDictionary.listNames(), 'maven-pro')}
      onClick={action('onClick')}>
      {text('label', 'Press me')}
    </WideButton>
  )
  .add('disabled', () => 
    <WideButton
      secondary={boolean('secondary', false)}
      white={boolean('white', false)}
      disabled={boolean('disabled', true)}
      isLoading={boolean('isLoading', false)}
      font={select('font', fontDictionary.listNames(), 'maven-pro')}
      onClick={action('onClick')}>
      {text('label', 'Press me')}
    </WideButton>
  )
  .add('secondary', () => 
    <WideButton
      secondary={boolean('secondary', true)}
      white={boolean('white', false)}
      disabled={boolean('disabled', false)}
      isLoading={boolean('isLoading', false)}
      font={select('font', fontDictionary.listNames(), 'maven-pro')}
      onClick={action('onClick')}>
      {text('label', 'Press me')}
    </WideButton>
  )
  .add('white', () => 
    <div style={{ padding: '5px', background: 'black' }}>
      <WideButton
        secondary={boolean('secondary', false)}
        white={boolean('white', true)}
        disabled={boolean('disabled', false)}
        isLoading={boolean('isLoading', false)}
        font={select('font', fontDictionary.listNames(), 'maven-pro')}
        onClick={action('onClick')}>
        {text('label', 'Press me')}
      </WideButton>
    </div>
  )