import React from 'react'
import { storiesOf } from '@storybook/react'
import { withKnobs, text, select, boolean } from '@storybook/addon-knobs'
import { action } from '@storybook/addon-actions'

import Button from './Button'

import { fontDictionary } from '../../../utils'

storiesOf('UI|Button/Button', module)
  .addDecorator(withKnobs)
  .add('default', () => 
    <Button 
      secondary={boolean('secondary', false)}
      white={boolean('white', false)}
      disabled={boolean('disabled', false)}
      isLoading={boolean('isLoading', false)}
      font={select('font', fontDictionary.listNames(), 'maven-pro')}
      onClick={action('onClick')}>
      {text('label', 'Press me')}
    </Button>
  )
  .add('disabled', () => 
    <Button
      secondary={boolean('secondary', false)}
      white={boolean('white', false)}
      disabled={boolean('disabled', true)}
      isLoading={boolean('isLoading', false)}
      font={select('font', fontDictionary.listNames(), 'maven-pro')}
      onClick={action('onClick')}>
      {text('label', 'Press me')}
    </Button>
  )
  .add('secondary', () => 
    <div style={{ padding: '5px' }}>
      <Button
        secondary={boolean('secondary', true)}
        white={boolean('white', false)}
        disabled={boolean('disabled', false)}
        isLoading={boolean('isLoading', false)}
        font={select('font', fontDictionary.listNames(), 'maven-pro')}
        onClick={action('onClick')}>
        {text('label', 'Press me')}
      </Button>
    </div>
  )
  .add('white', () => 
    <div style={{ padding: '5px', background: 'black' }}>
      <Button
        secondary={boolean('secondary', false)}
        white={boolean('white', true)}
        disabled={boolean('disabled', false)}
        isLoading={boolean('isLoading', false)}
        font={select('font', fontDictionary.listNames(), 'maven-pro')}
        onClick={action('onClick')}>
        {text('label', 'Press me')}
      </Button>
    </div>
  )
  .add('loading', () => 
    <Button
      secondary={boolean('secondary', false)}
      white={boolean('white', false)}
      disabled={boolean('disabled', false)}
      isLoading={boolean('isLoading', true)}
      font={select('font', fontDictionary.listNames(), 'maven-pro')}
      onClick={action('onClick')}>
      {text('label', 'Press me')}
    </Button>
  )