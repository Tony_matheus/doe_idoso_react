import React from 'react'
import { storiesOf } from '@storybook/react'
import { withKnobs, text, boolean } from '@storybook/addon-knobs'

import BorderButton from './BorderButton'

storiesOf('UI|Button/BorderButton', module)
  .addDecorator(withKnobs)
  .add('default', () => {
    const isWhite = boolean('white', false)    
    return (
      <div style={{ padding: '5px', background: isWhite ? 'black' : 'white' }}>
        <BorderButton 
          disabled={boolean('disabled', false)}
          white={isWhite}>
          {text('label', 'Press me')}
        </BorderButton>
      </div>
    )
  })
  .add('disabled', () => {
    const isWhite = boolean('white', false)    
    return (
      <div style={{ padding: '5px', background: isWhite ? 'black' : 'white' }}>
        <BorderButton 
          disabled={boolean('disabled', true)}
          white={isWhite}>
          {text('label', 'Press me')}
        </BorderButton>
      </div>
    )
  })
  .add('white', () => {
    const isWhite = boolean('white', true)    
    return (
      <div style={{ padding: '5px', background: isWhite ? 'black' : 'white' }}>
        <BorderButton 
          disabled={boolean('disabled', false)}
          white={isWhite}>
          {text('label', 'Press me')}
        </BorderButton>
      </div>
    )
  })