import React from 'react'
import styled from 'styled-components'

import { fontDictionary, vp } from '../../../utils'

const InnerLink = styled.a`
  cursor: pointer;
  text-decoration: none;
  font-size: ${vp.dk.vw(16)};
  ${fontDictionary.resolve}

  &:visited {
    color: black;
  }
  
  ${props => {
    if(props.disabled)
      return 'opacity: .5;'
    
    return `
      &:hover {
        opacity: .7;
      }
    `
  }}
`

const Link = ({ disabled, href, ...restProps }) => (
  <InnerLink
    {...restProps}
    disabled={disabled}
    href={disabled ? null : href}
    />
)

export default Link
