import styled from 'styled-components'
import PropTypes from 'prop-types'

import { Button }from 'antd'

import { vp } from '../../../utils'

const WideButton = styled(Button)`
  width: ${vp.dk.vw(450)};
  height: ${vp.dk.vw(44)};
  letter-spacing: 2px;
  font-weight: 600;

  @media only screen and (max-width: 1280px) {
    width: 300px;
    height: 29px;
  }
`

WideButton.propTypes = {
  font: PropTypes.string,
  onClick: PropTypes.func.isRequired,
  secondary: PropTypes.bool
}

export default WideButton
