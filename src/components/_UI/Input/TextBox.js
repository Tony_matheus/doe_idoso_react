import React from 'react'
import styled, { css } from 'styled-components'
import PropTypes from 'prop-types'
import InputMask from 'react-input-mask'

import Text from '../Text'

import { vp } from '../../../utils'

const Box = styled.div`
  width: ${vp.dk.vw(200)};
  //min-width: 133px;
  height: ${vp.dk.vw(37)};
  //min-height: 24px;
  border-bottom: 1px solid ${props => props.isWhite ? 'white' : 'black'};
  display: flex;
  align-items: center;
  position: relative;
  box-sizing: border-box;
  padding: 0 ${vp.dk.vw(12)};

  @media only screen and (max-width: 1280px) {
    width: 133px;
    height: 24px;
    min-height: 24px;
    padding: 0 8px;
  }
`

const Input = styled.input`
  background: transparent;
  font-size: ${vp.dk.vw(16)};
  outline: none;
  border: none;
  //border: 1px solid red;
  width: 100%;
  color: ${props => props.isWhite ? 'white' : 'black' };
  //line-height: ${vp.dk.vw(37)};
  ${props => props.moreMarginLeft && css`margin-left: ${vp.dk.vw(3)};`}
  ${props => props.moreMarginRight && css`margin-right: ${vp.dk.vw(3)};`}
  ${props => props.textAlign && css`text-align: ${props.textAlign};`}

  ::placeholder {
    font-style: italic;
    ${props => props.isWhite && 'color: #d6d6d6;' }
  }

  :-webkit-autofill,
  :-webkit-autofill:hover, 
  :-webkit-autofill:focus, 
  :-webkit-autofill:active  {
      -webkit-box-shadow: 0 0 0 30px ${props => props.isWhite ? 'black' : 'white'} inset !important;
  }

  :-webkit-autofill {
    -webkit-text-fill-color: ${props => props.isWhite ? 'white' : 'black'} !important;
}

  @media only screen and (max-width: 1280px) {
    font-size: 10px;
    color: ${props => props.isWhite ? 'white' : 'black' };
    //line-height: 24px;

    ${props => props.moreMarginLeft && css`margin-left: 2px;`}
    ${props => props.moreMarginRight && css`margin-right: 2px;`}
  }
`

const TextAdornment = styled(Text)`
  font-size: ${vp.dk.vw(16)};

  @media only screen and (max-width: 1280px) {
    font-size: 10px;
  }
`

const TextBox = props => {
  const { style, className, white, startAdornment, endAdornment, onClick, mask, ...restProps } = props
  
  const CurrentInput = mask
    ? (
      <InputMask 
        {...restProps}
        mask={mask}>
        {inputMaskProps => 
          <Input
            {...inputMaskProps}
            isWhite={white}
            moreMarginLeft={Boolean(startAdornment)}
            moreMarginRight={Boolean(endAdornment)}
            />
        }
      </InputMask>
    )
    : (
        <Input
          {...restProps}
          isWhite={white}
          moreMarginLeft={Boolean(startAdornment)}
          moreMarginRight={Boolean(endAdornment)}
          />
      )

  return (
    <Box
      style={style}
      className={className}
      isWhite={white}
      onClick={onClick}>
      {typeof startAdornment === 'string' || typeof startAdornment === 'number'
        ? <TextAdornment>{startAdornment}</TextAdornment>
        : startAdornment
      }
      { CurrentInput }
      {typeof endAdornment === 'string' || typeof endAdornment === 'number'
        ? <TextAdornment isWhite={white}>{endAdornment}</TextAdornment>
        : endAdornment
      }
    </Box>
  )
}

TextBox.propTypes = {
  placeholder: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string,
  startAdornment: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element
  ]),
  endAdornment: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element
  ]),
  textAlign: PropTypes.string
}

export default TextBox
