import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import { withKnobs, text, number, array } from '@storybook/addon-knobs'

import Select from './Select'

const data = Array(10).fill(1).map((_, index) => 'Option' + (index + 1))

storiesOf('UI|Input/Select', module)
  .addDecorator(withKnobs)
  .add('default', () => (
    <div style={{ padding: '5px', display: 'flex', flexDirection: 'column' }}>
      <Select
        placeholder={text('placeholder', 'Select an option:')}
        onToggle={action('onToggle')}
        data={array('data', data)}
        selectedIndex={number('selectedIndex', -1)}
        onOptionClick={action('onOptionClick')}
        />
        <h1>This message should be overlaid by options</h1>
    </div>
  ))
  .add('white', () => (
    <div style={{ padding: '5px', display: 'flex', flexDirection: 'column', background: 'black' }}>
      <Select
        white
        name="my-select"
        placeholder={text('placeholder', 'Select an option:')}
        onToggle={action('onToggle')}
        data={array('data', data)}
        selectedIndex={number('selectedIndex', -1)}
        onOptionClick={action('onOptionClick')}
        />
        <h1 style={{ color: 'white' }}>This message should be overlaid by options</h1>
    </div>
  ))