import React, { useState, useCallback, useRef } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { isNumber } from 'lodash'

import Text from '../Text'
import { Arrow } from '../Icon'

import { vp } from '../../../utils'

const Wrapper = styled.div.attrs({
  tabIndex: '1' //to turn div focusable and trigger onBlur event
})`
  display: flex;
  flex-direction: column;
  width: ${vp.dk.vw(452)};
  position: relative;
  outline: none;
  font-size: 40px;
  height: fit-content;

  @media only screen and (max-width: 1280px) {
    width: 301px;
  }
`

const BoxWrapper = styled.div.attrs({
  className: 'select-box-wrapper'
})`
  display: flex;
  align-items: center;
  width: 100%;
  background-color: ${props => props.isWhite ? 'black' : 'white'};
  cursor: default;
  border-radius: 4px;
  border: 1px solid ${props => props.isWhite ? 'white' : '#e1e1e1'};
  outline: 0px !important;
  height: ${vp.dk.vw(37)};
  position: relative;
  z-index: 2;
  border-bottom-left-radius: 4px;
  border-bottom-right-radius: 4px;
  transition: border-bottom-left-radius .3s linear .1s, border-bottom-right-radius .3s linear .1s;

  ${props => props.isOpen && `
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
    transition: border-bottom-left-radius .4s, border-bottom-right-radius .4s;
  `}

  @media only screen and (max-width: 1280px) {
    height: 24px;
  }
`

const SelectedOptionWrapper = styled.div`
  flex-grow: 1;
`

const StyledText = styled(Text).attrs({
  font: 'open-sans',
  className: 'select-text'
})`
  font-size: ${vp.dk.vw(14)};
  font-weight: 400;
  text-transform: uppercase;
  padding: 2px ${vp.dk.vw(20)};
  color: ${props => props.isWhite ? 'white' : 'black'};

  @media only screen and (max-width: 1280px) {
    font-size: 9px;
  }
`

const Placeholder = styled(StyledText)`
  //opacity: .7;
  ${props => props.isWhite && 'color: #d6d6d6'};
  font-style: italic;
`

// const SelectedOptionValue = styled(Placeholder)`
//   opacity: 1;
// `

const Option = styled(StyledText)`
  cursor: default;
  background: ${props => {
    if(props.isSelected) {
      return props.isWhite 
        ? '#5d5d5d'
        : '#e8e8e8'
    }

    return 'transparent'
  }};
  transition: background: .3s;

  &:hover {
    opacity: .5;  
  }
`

const ArrowBox = styled.div`
  width: ${vp.dk.vw(45)};
  height: 100%;
  border-left: 1px solid ${props => props.isWhite ? 'white' : '#e1e1e1'};
  display: flex;
  justify-content: center;
  align-items: center;

  @media only screen and (max-width: 1280px) {
    width: 30px;
  }
`

const StyledArrow = styled(Arrow).attrs(props => ({
  color: props.isWhite && 'white'
}))`
  width: 7px;
  height: 7px;
  transform: scaleY(${props => props.isOpen ? 1 : -1});
  transition: transform .4s;
`

const OptionsWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  overflow-y: scroll;
  max-height: ${props => props.isOpen ? 100 : 3}px;
  border: 1px solid ${props => props.isWhite ? 'white' : '#e1e1e1'};
  border-bottom-left-radius: 4px;
  border-bottom-right-radius: 4px;
  background: ${props => props.isWhite ? 'black' : 'white'};
  position: absolute;
  top: calc(100% - 4px);
  border-top: none;
  z-index: 1;
  transition: max-height .4s, background .4s, margin-top .4s;
  margin-top: ${props => props.isOpen ? 4 : 0}px;  
`

const Select = ({ placeholder, onToggle, onOptionClick, data, white, selectedIndex, onBlur, ...restProps }) => {
  const [isOpen, setIsOpen] = useState(false)
  const [idCloseTimeout, setIdCloseTimeout] = useState(null)
  const ref = useRef()

  const getName = useCallback(() => {
    const nameAttribute = ref.current && ref.current.attributes['name']
    return nameAttribute && nameAttribute.value
  }, [ref])  
  
  const onToggleInternal = useCallback(() => {
    const newValue = !isOpen

    setIsOpen(newValue)
    onToggle({ isOpen: newValue, ref, name: getName() })
  }, [isOpen, onToggle, getName])

  const stopClosing = useCallback(() => {
    if(idCloseTimeout) {
      clearTimeout(idCloseTimeout)
      setIdCloseTimeout(null)
    }
  }, [idCloseTimeout, setIdCloseTimeout])

  const close = useCallback(() => {
    stopClosing()
    onToggleInternal()
  },[stopClosing, onToggleInternal])

  const waitAndClose = useCallback(() => {
    if(isOpen) {
      const id = setTimeout(close, 2000)
      setIdCloseTimeout(id)
    }
  }, [isOpen, setIdCloseTimeout, close])

  const onOptionClickInternal = index => () => {
    onOptionClick({ index, ref, name: getName() })
    close()
  }

  const onBlurInternal = useCallback(() => { 
      isOpen && close()
      onBlur && onBlur()
    }, [isOpen, close, onBlur]
  )

  const currentData = (data || [])
  const selectedOption = isNumber(selectedIndex) 
    && selectedIndex >= 0 
    && selectedIndex < currentData.length
      ? currentData[selectedIndex]
      : ''

  return (
    <Wrapper 
      {...restProps}
      ref={ref}
      onMouseLeave={waitAndClose}
      onMouseEnter={stopClosing}
      onBlur={onBlurInternal}>
      <BoxWrapper
        onClick={onToggleInternal}
        isOpen={isOpen}
        isWhite={white}>
        <SelectedOptionWrapper>
          {!selectedOption 
            ? <Placeholder isWhite={white}>{placeholder}</Placeholder>
            : <StyledText isWhite={white}>{selectedOption}</StyledText>
          }
        </SelectedOptionWrapper>
        <ArrowBox>
          <StyledArrow 
            isOpen={isOpen}
            isWhite={white}
            />
        </ArrowBox>
      </BoxWrapper>
      <OptionsWrapper 
        isOpen={isOpen}
        isWhite={white}>
        {currentData.map((item, index) =>
          <Option
            key={item.id ? item.id : index}
            isSelected={index === selectedIndex}
            isWhite={white}
            onClick={onOptionClickInternal(index)}>
            {item.name ? item.name : item }
          </Option>
        )}
      </OptionsWrapper>
    </Wrapper>
  )
}

Select.defaultProps = {
  onToggle: () => {},
  onOptionClick: () => console.log('Not implemented'),
}

Select.propTypes = {
  onToggle: PropTypes.func,
  onOptionClick: PropTypes.func.isRequired,
  data: PropTypes.arrayOf(PropTypes.string).isRequired,
  white: PropTypes.bool,
  selectedIndex: PropTypes.number,
  onBlur: PropTypes.func,
}

export default Select
