import React from 'react'
import { storiesOf } from '@storybook/react'

import TextBox from './TextBox'

storiesOf('UI|Input/TextBox', module)
  .add('default', () =>
    <TextBox placeholder="Type something" />
  )
  .add('with start adornment', () => 
    <TextBox startAdornment="$" />
  )
  .add('with end adornment', () => 
    <TextBox endAdornment="Kg" />
  )
  .add('white', () => 
    <div style={{ padding: '5px', background: 'black' }}>
      <TextBox white />
    </div>
  )
