import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

import Divider from './Divider'
import Text from '../Text'

import { vp } from '../../../utils'

const Wrapper = styled.div`
  display: flex;
  align-items: center;

  & ${Divider} {
    flex-grow: 1;
  }
`

const Title = styled(Text)`
  text-transform: uppercase;
  margin 0 19px;
  font-size: ${vp.dk.vw(30)};
  font-weight: 400;
  letter-spacing: 5.1px;
  margin-right: 13.9px; //to compensate for the spacing in the last letter added by the letter-spacing

  @media only screen and (max-width: 1280px) {
    margin 0 12px;
    font-size: 20px;
    letter-spacing: 3.4px;
    margin-right: 8.6px; //to compensate for the spacing in the last letter added by the letter-spacing
  }
`

const SectionDivider = ({ children, ...restProps }) => (
  <Wrapper {...restProps}>
    <Divider />
    <Title>{children}</Title>
    <Divider />
  </Wrapper>
)

SectionDivider.defaultProps = {
  children: 'no title'
}

SectionDivider.propTypes = {
  children: PropTypes.string.isRequired
}

export default SectionDivider