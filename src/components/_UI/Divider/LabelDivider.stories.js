import React from 'react'
import { storiesOf } from '@storybook/react'
import { withKnobs, text } from '@storybook/addon-knobs'

import LabelDivider from './LabelDivider'

storiesOf('UI|Divider/LabelDivider', module)
  .addDecorator(withKnobs)
  .add('default', () => (
    <div style={{ padding: '5px' }}>
      <LabelDivider>{text('title', 'Cores')}</LabelDivider>
    </div>
  ))