import styled from 'styled-components'

const Divider = styled.div`
  background: #b2b2b2;
  display: flex;
  height: 1px;  
  margin-top: 2px;
  margin-bottom: 2px;
`

export default Divider