import React from 'react'
import { storiesOf } from '@storybook/react'

import Divider from './Divider'

storiesOf('UI|Divider/Divider', module)
  .add('default', () => (
    <Divider />
  ))