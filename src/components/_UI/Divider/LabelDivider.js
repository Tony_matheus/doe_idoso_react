import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

import Text from '../Text'
import Divider from '../Divider'

import { vp } from '../../../utils'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`

const Title = styled(Text).attrs({
  id: 'label-divider-title'
})`
  color: black;
  font-size: ${vp.dk.vw(15)};
  font-weight: 400;
  text-transform: uppercase;
  letter-spacing: 2.72px;
  margin-left: ${vp.dk.vw(3)};
  margin-bottom: ${vp.dk.vw(7)};

  @media only screen and (max-width: 1280px) {
    font-size: 10px;
    letter-spacing: 1.81px;
    margin-left: 2px;
    margin-bottom: 4px;
  }
`

const LabelDivider = ({ hideDivider, titleFont, children, ...restProps }) => (
  <Wrapper {...restProps}>
    <Title font={titleFont}>{children || 'No Title'}</Title>
    {!hideDivider && <Divider />}
  </Wrapper>
)

LabelDivider.defaultProps = {
  title: 'No Title',
  titleFont: 'maven-pro',
  hideDivider: false
}

LabelDivider.propTypes = {
  title: PropTypes.string.isRequired,
  hideDivider: PropTypes.bool
}

export default LabelDivider