import React from 'react'
import { storiesOf } from '@storybook/react'
import { withKnobs, text } from '@storybook/addon-knobs'

import SectionDivider from './SectionDivider'

storiesOf('UI|Divider/SectionDivider', module)
  .addDecorator(withKnobs)
  .add('default', () => (
    <div style={{ padding: '5px' }}>
      <SectionDivider>{text('title', 'Mais Vendidos')}</SectionDivider>
    </div>
  ))