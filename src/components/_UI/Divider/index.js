import Divider from './Divider'
import SectionDivider from './SectionDivider'
import LabelDivider from './LabelDivider'

export {
  Divider,
  SectionDivider,
  LabelDivider
}

export default Divider