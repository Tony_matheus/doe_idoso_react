import React from 'react'
import styled from 'styled-components'

import {FormModal} from '../../_UI/Modal'

import {Table} from 'antd';

import {Button} from "../../_UI/Button";

import {vp} from "../../../utils";

const Wrapper = styled.div``

const ButtonWrapper = styled.div`
  margin-bottom: ${vp.dk.vw(30)};
  
  @media only screen and (max-width: 1280px){
    margin-bottom: 20px;
  }
  
`

const AddButton = styled(Button)`
  background-color: #008000;
`

const ListCrud = (props) => {
  console.log(props.data, "props")
  const {
    columns,
    data,
    onPositiveButtonClick,
    onNegativeButtonClick,
    onFieldChangeModal,
    formModal,
    isModalVisible,
    modalTitle,
    isFormMultiple,
    onSelectOptionClick,
    onOpenModal,
    isLoading,
    createButtonTitle,
    ...restProps
  } = props

  console.warn(data, "props")
  console.log(formModal.formValuesModal)
  console.error(isLoading)
  return (
    <Wrapper {...restProps}>
      <ButtonWrapper>
        <AddButton onClick={() => onOpenModal()}>
          Adicionar {createButtonTitle}
        </AddButton>
      </ButtonWrapper>
      <Table
        loading={isLoading}
        columns={columns}
        dataSource={data}/>

      <FormModal
        onPositiveButtonClick={onPositiveButtonClick}
        onNegativeButtonClick={onNegativeButtonClick}
        positiveButtonLabel={"Salvar"}
        negativeButtonLabel={"Cancelar"}
        onFieldChange={onFieldChangeModal}
        fields={formModal.formFields}
        formValues={formModal.formValuesModal}
        values={formModal.formValuesModal}
        title={modalTitle}
        isVisible={isModalVisible}
        isFormMultiple={isFormMultiple}
        onSelectOptionClick={onSelectOptionClick}
        isLoading={isLoading}
      />
    </Wrapper>
  );
}

export default ListCrud;
