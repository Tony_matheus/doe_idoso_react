import styled from 'styled-components'
import PropTypes from 'prop-types'

import { fontDictionary, vp } from '../../../utils'

const Text = styled.p`
  margin: 0;
  font-size: ${vp.dk.vw(16)};
  ${fontDictionary.resolve}
`

Text.propTypes = {
  font: PropTypes.string
}

export default Text
