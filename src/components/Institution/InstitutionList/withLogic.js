import React from 'react'
import {Divider} from 'antd';
import {connect} from 'react-redux'

import {getAll, create, deleteInstitution} from '../../../redux/actions/institution'

const withConnect = Component => {
  const mapStateToProps = state => ({
    institution: state.institution,
    isGettingAll: state.institution.isGettingAll,
    isCreating: state.institution.isCreating,
    isDeleting: state.institution.isDeleting,
  })

  const actions = {
    getContent: getAll,
    create,
    deleteInstitution
  }
  return connect(mapStateToProps, actions)(Component)
}

const withLogic = Component => withConnect(class extends React.Component {
    constructor(props) {
      super(props)

      this.alertBarRef = React.createRef()

      this.state = {
        columns: [
          {
            title: 'Instituição',
            dataIndex: 'name',
            key: 'name',
          },
          {
            title: 'Descrição',
            dataIndex: 'description',
            key: 'description'
          },
          {
            title: 'Ações',
            key: 'action',
            render: (text, record) => (
              <span>
              <a href="#" onClick={() => this.handleOpenModal(record.id)}>Editar </a>
                <Divider type="vertical"/>
              <a href="#" onClick={() => this.handleDeleteInstitution(record.id)}>Deletar</a>
            </span>
            ),
          },
        ],
        data: Array(18).fill(1).map((_, index) => ({
          key: index,
          id: index,
          name: 'John Brown' + index,
          age: 32,
          description: 'New York No. 1 Lake Park',
          tags: ['nice', 'developer'],
        })),
        formModal: {
          formFields: [],
          formValues: {},
          isModalVisible: true,
          isMultiple: true,
        }
      }
    }

    render() {
      return (
        <Component
          columns={this.state.columns}
          data={this.props.institution.all}
          isLoading={this.props.isGettingAll || this.props.isCreating || this.props.isDeleting }
          isFormMultiple={this.state.isMultiple}
          modalTitle={this.state.formModal.title}
          isModalVisible={this.state.isModalVisible}
          formModal={this.state.formModal}
          onPositiveButtonClick={this.handlePositiveButtonClick}
          onNegativeButtonClick={this.handleNegativeButtonClick}
          onSelectOptionClick={this.handleSelectionOptionClick}
          onFieldChangeModal={this.handleFieldChangeModal}
          onOpenModal={this.handleOpenModal}
        />
      )
    }

    componentDidMount() {
      this.props.getContent()
    }

    componentDidUpdate(prevProps) {
      
    }

    handleOpenModal = (id = null) => {
      console.log(id)
      this.setState(state => ({
        isModalVisible: true,
        isMultiple: true,
        formModal: {
          ...state.formModal,
          formFields: [
            {
              title: "Institution",
              fields: [
                {
                  name: 'name',
                  placeholder: 'Nome'
                },
              ]
            },
            {
              title: "Endereço de entrega",
              fields: [
                {
                  name: 'zipCode',
                  placeholder: 'CEP'
                },
                {
                  name: 'street',
                  placeholder: 'Rua'
                },
                {
                  name: 'number',
                  placeholder: 'Numero'
                },
                {
                  name: 'district',
                  placeholder: 'Bairro'
                },
                {
                  name: 'complement',
                  placeholder: 'Complemento'
                },
                {
                  name: 'city',
                  placeholder: 'Cidade'
                },
                {
                  name: 'state',
                  placeholder: 'Estado'
                },
              ]
            },
            {
              title: "Contato",
              fields: [
                {
                  name: 'contact_name',
                  placeholder: 'Nome do Contato'
                },
                {
                  name: 'email',
                  placeholder: 'E-Mail'
                },
                {
                  name: 'phone',
                  placeholder: 'Numero de Telefone'
                },
                {
                  name: 'areaCode',
                  placeholder: 'DDD'
                }
              ]
            }
          ],
          formValuesModal:
            (id !== null)
              // ? {
              //   name: "Teste NUll",
              //   description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
              //   neighborhood: "Edson queiroz",
              //   city: "fortaleza",
              //   complement: "Bloco A",
              //   postal_code: "12345-789",
              //   district: "unifor",
              //   number: 1234,
              //   state: "unifor",
              //   street: "unifor",
              //   zip_code: "12345-789",
              //   contact_name: "unifor",
              //   email: "unifor@unifor.com",
              //   phone: "12345678",
              //   areaCode: "123"
              // }
              ? this.handleEditInstitution(id)
              : {
                name: "",
                description: "",
                neighborhood: "",
                city: "",
                complement: "",
                postal_code: "",
                district: "",
                number: "",
                state: "",
                street: "",
                zip_code: "",
                contact_name: "",
                email: "",
                phone: "",
                areaCode: ""
              },
          title: (id)
            ? "Editando Instituição: "
            : 'Criando Instituição'
        }
      }))
    }

    handleFieldChangeModal = event => {
      const fieldName = event.target.name
      const fieldValue = event.target.value

      this.setState(state => ({
        formModal: {
          ...state.formModal,
          formValuesModal: {
            ...state.formModal.formValuesModal,
            [fieldName]: fieldValue
          }
        }
      }))
    }

    handlePositiveButtonClick = () => {
      console.log(this.state.formModal.formValuesModal)
      this.props.create(this.state.formModal.formValuesModal)
      this.setState(state => ({
        ...state,
        // isModalVisible: false
      }))
    }

    handleNegativeButtonClick = () => {
      this.setState(state => ({
        ...state,
        isModalVisible: false
      }))
    }

    handleEditInstitution = (institutionId) => {
      const institution = this.props.institution.all.filter(inst => inst.id === institutionId)
      return {
        name: institution[0].name,
        description: institution[0].description,
        neighborhood: institution[0].address.neighborhood,
        city: institution[0].address.city,
        complement: institution[0].address.complement,
        postal_code: institution[0].address.postal_code,
        district: institution[0].address.district,
        number: institution[0].address.number,
        state: institution[0].address.state,
        street: institution[0].address.street,
        zipCode: institution[0].address.zip_code,
        contact_name: institution[0].contacts[0].contact_name,
        email: institution[0].contacts[0].email,
        phone: institution[0].contacts[0].phone,
        areaCode: institution[0].contacts[0].areaCode
      }
    }

    handleDeleteInstitution = (institutionId) => {
      this.props.deleteInstitution(institutionId)
    }


    handleSelectionOptionClick = ({index, name}) => {
      this.setState(state => ({
        formValues: {
          ...state.formValues,
          [name]: index
        }
      }))
    }
  }
)

export default withLogic
