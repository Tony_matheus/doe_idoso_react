import React from "react";
import styled from "styled-components/macro";

import { FormModal } from "../../_UI/Modal";

import { Table } from "antd";

import withLogic from "./withLogic";
import { Button } from "../../_UI/Button";

import { vp } from "../../../utils";
import ListCrud from "../../_UI/ListCrud";

const Wrapper = styled.div``;

const ButtonWrapper = styled.div`
  margin-bottom: ${vp.dk.vw(30)};

  @media only screen and (max-width: 1280px) {
    margin-bottom: 20px;
  }
`;

const AddButton = styled(Button)`
  background-color: #008000;
`;

const InstitutionList = props => {
  const {
    columns,
    data,
    onPositiveButtonClick,
    onNegativeButtonClick,
    onFieldChangeModal,
    formModal,
    isModalVisible,
    modalTitle,
    isFormMultiple,
    onSelectOptionClick,
    onOpenModal,
    isLoading,
    createButtonTitle,
    ...restProps
  } = props;

  // console.log(formModal.formValuesModal)
  return <ListCrud {...props} />;
};

export { InstitutionList };

export default withLogic(InstitutionList);
