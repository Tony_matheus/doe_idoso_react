import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  background: white;
  width: 60vw;
  height: 100vh;
  justify-content: center;
  align-items: center;
`

const AuthAside = () => (
  <Wrapper/>
)

export default AuthAside
