import React from 'react'
import { storiesOf } from '@storybook/react'

import Login from './'

const lorem = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ultrices nibh vel leo ullamcorper fermentum. Proin vitae sem tincidunt, tempor tortor quis, varius purus. Suspendisse iaculis luctus lorem non aliquet.'

storiesOf('Auth|LoginForm/LoginForm Form', module)
  .add('default', () =>
    <div style={{ paddingLeft: '50px' }}>
      <Login/>
    </div>
  )
