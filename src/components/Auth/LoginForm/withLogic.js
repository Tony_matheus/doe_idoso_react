import React from 'react'
import isEmail from 'validator/lib/isEmail'
import { connect } from 'react-redux'
import { omit } from 'lodash'
import { Redirect, navigate } from '@reach/router'
import { message } from 'antd';
import { authenticate } from '../../../redux/actions/user'

const withConnect = Component => {
  const mapStateToProps = state => ({ user: state.user })
  const actions = { authenticate }

  return connect(
    mapStateToProps,
    actions
  )(Component)
}

const withLogic = Component => withConnect(class extends React.Component {
  constructor(props) {
    super(props)

    this.alertBarRef = React.createRef()

    this.state = {
      email: '',
      password: '',
      errorMessage: ''
    }
  }

  render() {
    console.log(this.props, "propss")
    const { user: { isAuthenticating, isAuthenticated }, ...restProps } = this.props
    const whiteListedRestProps = omit(restProps, ['authenticate'])

    return (
      isAuthenticated
        ? <Redirect
          noThrow
          to="/institution"
        />
        :
        <Component
          values={this.state}
          authenticating={isAuthenticating}
          onFieldChange={this.handleFieldChange}
          onSubmit={this.handleSubmit}
          onForgotPasswordButtonClick={this.handleForgotPasswordButtonClick}
          onSignInButtonClick={this.handleSignInButtonClick}
          alertBarRef={this.alertBarRef}
        />
    )
  }

  componentDidUpdate(prevProps) {
    if (!prevProps.user.error && this.props.user.error)      
      message.error('Falha ao fazer login');
    if (!prevProps.user.isAuthenticated && this.props.user.isAuthenticated) {
      console.log(this.props.user.isAuthenticated)
      message.success('Logado com Sucesso');
    }
  }

  handleFieldChange = event => {
    this.setState({ [event.target.name]: event.target.value })
  }

  test = event => {

  }

  handleSubmit = event => {
    // event.preventDefault()

    // this.hideAlert()

    const { email, password } = this.state

    if (!email)
      return this.showAlert('Informe seu email')

    if (!password)
      return this.showAlert('Informe sua senha')

    if (!isEmail(email))
      return this.showAlert('Email inválido')

    // alert("Not Implemented")
    this.props.authenticate({ email, password })
  }

  handleForgotPasswordButtonClick = () => {
    alert('Not implemented')
  }

  handleSignInButtonClick = () => {
    // this.props.clearError()
    navigate('sign-up')
  }

  showAlert = message => {
    // this.alertBarRef.current.show(message)
  }

  hideAlert = () => {
    this.alertBarRef.current.dismiss()
  }
})

export default withLogic
