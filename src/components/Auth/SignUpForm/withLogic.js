import React from 'react'
import isEmail from 'validator/lib/isEmail'
import { connect } from 'react-redux'
import { omit } from 'lodash'
import { Redirect, navigate } from '@reach/router'
import { message } from 'antd';
import { register } from '../../../redux/actions/user'

const withConnect = Component => {
  const mapStateToProps = state => ({ user: state.user })
  const actions = { register }

  return connect(
    mapStateToProps,
    actions
  )(Component)
}

const withLogic = Component => withConnect(class extends React.Component {
  constructor(props) {
    super(props)

    this.alertBarRef = React.createRef()

    this.state = {
      email: '',
      password: '',
      password_confirmation: '',
      errorMessage: ''
    }
  }

  render() {
    console.log(this.props, "propss")
    const { user: { isAuthenticating, isAuthenticated }, ...restProps } = this.props
    const whiteListedRestProps = omit(restProps, ['authenticate'])

    return (
      isAuthenticated
        ? <Redirect
          noThrow
          to="/institution"
        />
        :
        <Component
          values={this.state}
          authenticating={isAuthenticating}
          onFieldChange={this.handleFieldChange}
          onSubmit={this.handleSubmit}
          onForgotPasswordButtonClick={this.handleForgotPasswordButtonClick}
          onSignInButtonClick={this.handleSignInButtonClick}
          alertBarRef={this.alertBarRef}
        />
    )
  }

  componentDidUpdate(prevProps) {
    if (!prevProps.user.error && this.props.user.error) {
      console.log(this.props.user.error)
      message.error('Usuário não registrado');
    }
    if (!prevProps.user.registered && this.props.user.registered) {
      console.log(this.props.user.registered)
      message.success('Registrado com Sucesso');
      navigate("/")
    }

  }

  handleFieldChange = event => {
    console.log(this.state)
    console.log(event.target.name)
    console.log(event.target.value)
    this.setState({ [event.target.name]: event.target.value })
  }

  test = event => {

  }

  handleSubmit = event => {
    // event.preventDefault()

    // this.hideAlert()

    const { email, password, password_confirmation } = this.state
    console.log(password_confirmation)
    // if (!email)
    //   return message.error('Informe seu email')

    // if (!password) {
    //   return message.error('Informe sua senha')
    // }
    // if (!password_confirmation) {
    //   return message.error('Informe sua confirmação de senha')
    // }
    // if (password.trim() !== password_confirmation.trim()) {
    //   return message.error('Senhas não batem');
    // }
    if (!isEmail(email)) {
      return message.error('Email inválido')
    }
    // alert("Not Implemented")
    this.props.register({ email, password, password_confirmation })
  }

  handleForgotPasswordButtonClick = () => {
    alert('Not implemented')
  }

  handleSignInButtonClick = () => {
    // this.props.clearError()
    navigate('/')
  }

  showAlert = message => {
    // this.alertBarRef.current.show(message)
  }

  hideAlert = () => {
    this.alertBarRef.current.dismiss()
  }
})

export default withLogic
