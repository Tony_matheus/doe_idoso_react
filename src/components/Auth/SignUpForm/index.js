import React from 'react'
import styled from 'styled-components'

import { Input, Button } from 'antd';

import { Link, WideButton } from '../../_UI/Button'

import { vp } from '../../../utils'

import withLogic from './withLogic'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background: black;
  width: 50%;
  position: relative;
  
`

const InnerWrapper = styled.div`
  display: inline-flex;
  flex-direction: column;
  align-items: center;
  //border: 1px solid white;
`

const FormWrapper = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: ${vp.dk.vw(383)};
  position: relative;

  @media only screen and (max-width: 1280px) {
    width: 255px;
  }
`

const EmailInput = styled(Input).attrs({
  name: "email",
  type: "text",
  placeholder: "E-Mail"
})`
  margin-bottom: ${vp.dk.vw(20)};
  width: ${vp.dk.vw(450)};
  background-color: black;
  color: white;
  
  @media only screen and (max-width: 1280px){
    margin-bottom: 13px;
    width: 300px;
  }
`

const PasswordInput = styled(Input.Password)`
  margin-bottom: ${vp.dk.vw(20)};
  width: ${vp.dk.vw(450)};
  input{
    background-color: black !important;
    color: white !important;
  }
  span{
    i{
      color: white;
    }
  }
  @media only screen and (max-width: 1280px){
    margin-bottom: 13px;
    width: 300px;
  }
`

const LinksWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
  margin-top: ${vp.dk.vw(11)};

  @media only screen and (max-width: 1280px) {
    margin-top: 7px;
  }
`

const StyledLink = styled(Link)`
  color: white;
  font-size: ${vp.dk.vw(17)};
  font-weight: 500;
  margin-${props => props.isRight ? 'right' : 'left'}: ${vp.dk.vw(5)};

  &:visited {
    color: white;
  }

  @media only screen and (max-width: 1280px) {
    font-size: 11px;
    margin-${props => props.isRight ? 'right' : 'left'}: 3px;
  }
`

const EnterButton = styled(Button)`
  background-color: black;
  color: white;
  margin-top: ${vp.dk.vw(20)};
  font-size: ${vp.dk.vw(17)};
  width: ${vp.dk.vw(450)};
  height: ${vp.dk.vw(44)};
  letter-spacing: 2px;
  font-weight: 600;

  @media only screen and (max-width: 1280px) {
    width: 300px;
    height: 29px;
  }

  /* &:active{
    background-color: white;
    color: black;
  } */

  @media only screen and (max-width: 1280px) {
    margin-top: 13px;
  }
`

const SignUpForm = ({ onFieldChange, onSignInButtonClick, authenticating, onSubmit, test, values, ...restProps }) => (
  <Wrapper {...restProps}>
    {console.log(authenticating)}
    <InnerWrapper>
      <FormWrapper>
        <EmailInput
          name="email"
          //value={values.email}
          onChange={onFieldChange}
        />
        <PasswordInput
          name="password"
          type="password"
          //value={values.password}
          placeholder="Senha"
          onChange={onFieldChange}
        />
        <PasswordInput
          name="password_confirmation"
          type="password"
          //value={values.password_confirmation}
          placeholder="confirmação de senha"
          onChange={onFieldChange}
        />
        <LinksWrapper>
          <StyledLink
            onClick={onSignInButtonClick}>
            logar-se
          </StyledLink>
        </LinksWrapper>
        <EnterButton
          loading={authenticating}
        onClick={onSubmit}
        >
          Cadastrar-se
        </EnterButton>
      </FormWrapper>
    </InnerWrapper>
  </Wrapper>
)

export default withLogic(SignUpForm)
