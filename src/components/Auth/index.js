import LoginForm from './LoginForm'
import SignUpForm from './SignUpForm'
import AuthAside from './AuthAside'

export {
  LoginForm,
  SignUpForm,
  AuthAside
}