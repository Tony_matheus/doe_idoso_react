import React from 'react'
import styled from 'styled-components'

import {FormModal} from '../../_UI/Modal'

import {Table} from 'antd/lib/index';

import withLogic from './withLogic'
import {Button} from "../../_UI/Button";

import {vp} from "../../../utils";
import ListCrud from "../../_UI/ListCrud";

const Wrapper = styled.div``

const ButtonWrapper = styled.div`
  margin-bottom: ${vp.dk.vw(30)};
  
  @media only screen and (max-width: 1280px){
    margin-bottom: 20px;
  }
  
`

const AddButton = styled(Button)`
  background-color: #008000;
`

const ProjectList = (props) => {
  const {
    columns,
    data,
    onPositiveButtonClick,
    onNegativeButtonClick,
    onFieldChangeModal,
    formModal,
    isModalVisible,
    modalTitle,
    isFormMultiple,
    onSelectOptionClick,
    onOpenModal,
    isLoading,
    createButtonTitle,
    ...restProps
  } = props

  return (
    <ListCrud {...props}/>
  );
}

export {ProjectList};

export default withLogic(ProjectList);
