import React from "react";
import styled from "styled-components/macro";

import ListCrud from "../../_UI/ListCrud";
import { vp } from "../../../utils";

import { Button } from "../../_UI/Button";
import withLogic from "./withLogic";

const ArticleList = props => {
  const {
    columns,
    data,
    onPositiveButtonClick,
    onNegativeButtonClick,
    onFieldChangeModal,
    formModal,
    isModalVisible,
    modalTitle,
    isFormMultiple,
    onSelectOptionClick,
    onOpenModal,
    isLoading,
    createButtonTitle,
    ...restProps
  } = props;

  return <ListCrud {...props} />;
};

export {ArticleList};

export default withLogic(ArticleList);
