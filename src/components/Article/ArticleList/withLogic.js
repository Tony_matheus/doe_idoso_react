import React from "react";
import { Divider } from "antd";
import { connect } from "react-redux";

import { getAll, create, deleteArticle } from "../../../redux/actions/article";

const withConnect = Component => {
  const mapStateToProps = state => ({
    article: state.article,
    isGettingAll: state.article.isGettingAll,
    isCreating: state.article.isCreating,
    isDeleting: state.article.isDeleting
  });

  const actions = {
    getContent: getAll,
    create,
    deleteArticle
  };
  return connect(
    mapStateToProps,
    actions
  )(Component);
};

const withLogic = Component =>
  withConnect(
    class extends React.Component {
      constructor(props) {
        super(props);

        this.alertBarRef = React.createRef();

        this.state = {
          columns: [
            {
              title: "Titulo",
              dataIndex: "title",
              key: "title"
            },
            {
              title: "Texto",
              dataIndex: "text",
              key: "text"
            },
            {
              title: "Action",
              key: "action",
              render: (text, record) => (
                <span>
                  <a href="#" onClick={() => this.handleOpenModal(record.id)}>
                    Editar{" "}
                  </a>
                  <Divider type="vertical" />
                  <a
                    href="#"
                    onClick={() => this.handleDeleteArticle(record.id)}
                  >
                    Deletar
                  </a>
                </span>
              )
            }
          ],
          data: Array(18)
            .fill(1)
            .map((_, index) => ({
              key: index,
              id: index,
              name: "John Brown" + index,
              age: 32,
              description: "New York No. 1 Lake Park",
              tags: ["nice", "developer"]
            })),
          formModal: {
            formFields: [],
            formValues: {},
            isModalVisible: true,
            isMultiple: true
          }
        };
      }

      render() {
        return (
          <Component
            columns={this.state.columns}
            data={this.props.article.all}
            isLoading={
              this.props.isGettingAll ||
              this.props.isCreating ||
              this.props.isDeleting
            }
            isFormMultiple={this.state.isMultiple}
            modalTitle={this.state.formModal.title}
            isModalVisible={this.state.isModalVisible}
            formModal={this.state.formModal}
            onPositiveButtonClick={this.handlePositiveButtonClick}
            onNegativeButtonClick={this.handleNegativeButtonClick}
            onSelectOptionClick={this.handleSelectionOptionClick}
            onFieldChangeModal={this.handleFieldChangeModal}
            onOpenModal={this.handleOpenModal}
          />
        );
      }

      componentDidMount() {
        this.props.getContent();
      }

      componentDidUpdate(prevProps) { }

      handleOpenModal = (id = null) => {
        console.log(id);
        this.setState(state => ({
          isModalVisible: true,
          isMultiple: true,
          formModal: {
            ...state.formModal,
            formFields: [
              {
                title: "Noticia",
                fields: [
                  {
                    name: "title",
                    placeholder: "Titulo"
                  },
                  {
                    name: "text",
                    placeholder: "Texto"
                  }
                ]
              }
            ],
            formValuesModal:
              id !== null
                ? this.handleEditArticle(id)
                : {
                  title: "",
                  text:
                    ""
                },
            title: id ? "Editando Noticia: " : "Criando Noticia"
          }
        }));
      };

      handleFieldChangeModal = event => {
        const fieldName = event.target.name;
        const fieldValue = event.target.value;

        this.setState(state => ({
          formModal: {
            ...state.formModal,
            formValuesModal: {
              ...state.formModal.formValuesModal,
              [fieldName]: fieldValue
            }
          }
        }));
      };

      handlePositiveButtonClick = () => {
        console.log(this.state.formModal.formValuesModal);
        this.props.create(this.state.formModal.formValuesModal);
        this.setState(state => ({
          ...state,
          isModalVisible: false
        }));
      };

      handleNegativeButtonClick = () => {
        this.setState(state => ({
          ...state,
          isModalVisible: false
        }));
      };

      handleEditArticle = articleId =>
        this.props.article.all.find(art => art.id === articleId);

      handleDeleteArticle = articleId => {
        this.props.deletearticle(articleId);
      };

      handleSelectionOptionClick = ({ index, name }) => {
        this.setState(state => ({
          formValues: {
            ...state.formValues,
            [name]: index
          }
        }));
      };
    }
  );

export default withLogic;
