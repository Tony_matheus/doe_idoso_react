import React, { useEffect } from 'react';
import { Router } from '@reach/router';
import ReactGA from 'react-ga'
import 'antd/dist/antd.css';
import { Login, User, SignUp, Institution, Project, Article, Donate } from './pages'

import { OnRouteChange } from './components/_Misc/Route'
import withStoreProvider from './redux/withStoreProvider'

function App() {
  useEffect(() => {
    ReactGA.initialize('UA-139051132-1')
  }, [])

  return (
    <>
      <Router>
        <Login path="/" />
        <SignUp path="/sign-up" />
        <Institution path="institution" />
        <Project path="project" />
        <Article path="article" />
        <Donate path="donate" />
        <User path="/users"/>
      </Router>
      <OnRouteChange action={() => {
        window.scrollTo(0, 0)
        ReactGA.pageview(window.location.pathname + window.location.search)
      }} />
    </>
  );
}

export default withStoreProvider(App);
